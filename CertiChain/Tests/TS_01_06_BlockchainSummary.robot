*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing
Documentation  Suite description
Resource  Elements.robot
Resource  Buttons.robot
Resource  InputFields.robot
Resource  Browser_links.robot
Resource  Keywords.robot
Resource  CustomVariables.robot

*** Test Cases ***
TC_01_06_01 - Show Records In Blockchain
    open chrome and login test user
    Press Key   xpath=//body  \ue00f # press page down
    click link  ${UserHomeBtnBlockChainSummary}
    wait until element is visible  ${BlockchainSummaryTitleElement}
    element should be visible  ${BlockchainSummaryFirstItem}

TC_01_06_01 - Postcondition
    close browser

TC_01_06_02 - Verify Last Record
    open chrome and login test user
    Press Key   xpath=//body  \ue00f # press page down
    click link  ${UserHomeBtnBlockChainSummary}
    wait until element is visible  ${BlockchainSummaryTitleElement}
    Press Key   xpath=//body  \ue00f # press page down
    Press Key   xpath=//body  \ue00f # press page down
    click button  ${BlockchainSummaryCheckItemBtn}
    wait until element is visible  ${BlockchainSummaryVerifySuccess}

TC_01_06_02 - Postcondition
    close browser

TC_01_06_03 - Filter Records In Blockchain Summary
    open chrome and login test user
    Press Key   xpath=//body  \ue00f # press page down
    click link  ${UserHomeBtnBlockChainSummary}
    wait until element is visible  ${BlockchainSummaryTitleElement}
    put text into field  10.05.2022  ${BlockchainSummaryDeFilterFrom}  ${BlockchainSummaryTitleElement}
    put text into field  15.05.2022  ${BlockchainSummaryDeFilterTo}  ${BlockchainSummaryTitleElement}
    click button  ${BlockchainSummaryFilterBtn}
    wait until element is visible  ${BlockchainSummaryFilterElementResult}

TC_01_06_03 - Postcondition
    close browser


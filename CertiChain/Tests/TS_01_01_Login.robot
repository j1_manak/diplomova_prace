*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing
Documentation    Suite description
Resource  Elements.robot
Resource  Buttons.robot
Resource  InputFields.robot
Resource  Browser_links.robot
Resource  Keywords.robot

*** Test Cases ***
TC_01_01_01 - Success Login
    Open Chrome And Page
    Click Link  ${NavBarLogin}
    Wait Until Element Is Visible  ${LoginTitle}
    Put Text Into Field  ${LoginMailVal}  ${LoginEmail}  ${LoginTitle}
    Put Text Into Field  ${LoginPwdVal}  ${LoginPwd}  ${LoginTitle}
    Click Button  ${BtnLogin}
    Wait Until Element Is Visible  ${UserHomeElement}

TC_01_01_01 - Postcondition
    Close Browser

TC_01_01_02 - Wrong Pwd
    Open Chrome And Page
    Click Link  ${NavBarLogin}
    Wait Until Element Is Visible  ${LoginTitle}
    Put Text Into Field  ${LoginMailVal}  ${LoginEmail}  ${LoginTitle}
    Put Text Into Field  badpwd123  ${LoginPwd}  ${LoginTitle}
    Click Button  ${BtnLogin}
    wait until element is visible  ${LoginBadEmailPwd}
    element should be visible  ${LoginBadEmailPwd}

TC_01_01_02 - Postcondition
    Close Browser

TC_01_01_03 - Wrong Email
    Open Chrome And Page
    Click Link  ${NavBarLogin}
    Wait Until Element Is Visible  ${LoginTitle}
    Put Text Into Field  notExistinEmail@email.cz  ${LoginEmail}  ${LoginTitle}
    Put Text Into Field  ${LoginPwdVal}  ${LoginPwd}  ${LoginTitle}
    Click Button  ${BtnLogin}
    wait until element is visible  ${LoginBadEmailPwd}
    element should be visible  ${LoginBadEmailPwd}

TC_01_01_03 - Postcondition
    Close Browser

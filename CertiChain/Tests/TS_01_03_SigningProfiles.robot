*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing
Documentation    Suite description
Resource  Elements.robot
Resource  Buttons.robot
Resource  InputFields.robot
Resource  Browser_links.robot
Resource  Keywords.robot

*** Test Cases ***
TC_01_03_01 - Create Cades Signature
    Open Chrome And Login Test User
    click link  ${UserHomeBtnSignaturesManagenent}
    wait until element is visible  ${ProfileManagementBtnCreate}
    click link  ${ProfileManagementBtnCreate}
    Put Text Into Field  Test-cades  ${CreateSigProfNameField}  ${CreateSigProfProfName}
    Select From List By Index  ${CreateSigProfSigTypeSelect}  1
    select from list by index  ${CreateSigProfHashAlgSelect}  2
    click button  ${CreateSigProfBtnNext}
    wait until element is visible  ${CreateSigProfCadesTypeSelect}
    select from list by index  ${CreateSigProfCadesTypeSelect}  0
    select from list by index  ${CreateSigProfCadesDetachedSelect}  0
    click button  ${CreateSigProfBtnSave}
    wait until element is visible  ${ProfileManagementBtnCreate}

TC_01_03_01 - Postcondition
    close browser

TC_01_03_02 - Create Xades Signature
    Open Chrome And Login Test User
    click link  ${UserHomeBtnSignaturesManagenent}
    wait until element is visible  ${ProfileManagementBtnCreate}
    click link  ${ProfileManagementBtnCreate}
    Put Text Into Field  Test-Xades  ${CreateSigProfNameField}  ${CreateSigProfProfName}
    Select From List By Index  ${CreateSigProfSigTypeSelect}  2
    select from list by index  ${CreateSigProfHashAlgSelect}  3
    click button  ${CreateSigProfBtnNext}
    wait until element is visible  ${CreateSigProfXadesTypeSelect}
    select from list by index  ${CreateSigProfXadesTypeSelect}  0
    select from list by index  ${CreateSigProfXadesDetachedSelect}  1
    click button  ${CreateSigProfBtnSave}
    wait until element is visible  ${ProfileManagementBtnCreate}

TC_01_03_02 - Postcondition
   close browser

TC_01_03_03 - Create Pades Invisible
    Open Chrome And Login Test User
    click link  ${UserHomeBtnSignaturesManagenent}
    wait until element is visible  ${ProfileManagementBtnCreate}
    click link  ${ProfileManagementBtnCreate}
    Put Text Into Field  Test-Pades-Invisibled  ${CreateSigProfNameField}  ${CreateSigProfProfName}
    Select From List By Index  ${CreateSigProfSigTypeSelect}  0
    select from list by index  ${CreateSigProfHashAlgSelect}  3
    click button  ${CreateSigProfBtnNext}
    Put Text Into Field  Test-Robot  ${CreateSigProfSigneNameField}  ${CreateSigPadesType}
    PUT TEXT INTO FIELD  Zlín  ${CreateSigProfSignLocField}  ${CreateSigPadesType}
    Put Text Into Field  Auto test  ${CreateSigProfSignReasonField}  ${CreateSigPadesType}
    click button  ${CreateSigProfBtnSave}
    wait until element is visible  ${ProfileManagementBtnCreate}

TC_01_03_03 - Postcondition
   close browser

TC_01_03_04 - Create Pades Unvisible Add Signer Info
    Open Chrome And Login Test User
    click link  ${UserHomeBtnSignaturesManagenent}
    wait until element is visible  ${ProfileManagementBtnCreate}
    click link  ${ProfileManagementBtnCreate}
    Put Text Into Field  Test-Pades-Invisible-SignerInfo  ${CreateSigProfNameField}  ${CreateSigProfProfName}
    Select From List By Index  ${CreateSigProfSigTypeSelect}  0
    select from list by index  ${CreateSigProfHashAlgSelect}  3
    click element  ${CreateSigProfAddSignerInfo}
    wait until element is visible  ${CreateSigProfCountrySelect}
    select from list by index  ${CreateSigProfCountrySelect}  0
    put text into field  Zlínský  ${CreateSigProfCityField}  ${CreateSigProfProfName}
    put text into field  76000  ${CreateSigProfZipCodeField}  ${CreateSigProfProfName}
    put text into field  Zlín  ${CreateSigProfCityField}  ${CreateSigProfProfName}
    put text into field  Robot test  ${CreateSigProfRoleField}  ${CreateSigProfProfName}
    click element  ${CreateSigProfCreationProofCheck}
    click element  ${CreateSigProfOwnerProofCheck}
    click button  ${CreateSigProfBtnSave}
    wait until element is visible  ${ProfileManagementBtnCreate}

TC_01_03_04 - Postcondition
   close browser
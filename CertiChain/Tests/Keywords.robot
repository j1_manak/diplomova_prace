*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing
Resource  Browser_links.robot
Resource  Elements.robot
Resource  CustomVariables.robot

*** Keywords ***

Open Chrome And Page
    Set Selenium Speed  0.2
    Set Selenium Timeout  30
    Open Browser  ${RealUrl}  chrome
    Wait Until Element Is Visible  ${WelcomeScreenElement}

Open Chrome Page Go Register
    Open Chrome And Page
    Click Link  ${NavBarRegister}
    Wait Until Element Is Visible  ${RegisterTitleElement}

Open Chrome And Login Test User
    Open Chrome And Page
    Click Link  ${NavBarLogin}
    Wait Until Element Is Visible  ${LoginTitle}
    Put Text Into Field  ${LoginMailVal}  ${LoginEmail}  ${LoginTitle}
    Put Text Into Field  ${LoginPwdVal}  ${LoginPwd}  ${LoginTitle}
    Click Button  ${BtnLogin}
    Wait Until Element Is Visible  ${UserHomeElement}


Put Text Into Field
    [Arguments]  ${text}  ${XpathInputField}  ${XpathImage}
    Wait Until Element Is Visible  ${XpathInputField}
    Input Text  ${XpathInputField}  ${text}
    Click Element  ${XpathImage}


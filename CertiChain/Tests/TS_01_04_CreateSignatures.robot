*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing
Documentation  Suite description
Resource  Elements.robot
Resource  Buttons.robot
Resource  InputFields.robot
Resource  Browser_links.robot
Resource  Keywords.robot
Resource  CustomVariables.robot

*** Test Cases ***
TC_01_04_01 - Create Cades
   Open Chrome And Login Test User
   click link  ${UserHomeBtnCreateSignature}
   wait until element is visible  ${CreateSigElement}
   click element  ${CreateSigCadesProfile}
   click button  ${CreateSigBtnContinue}
   wait until element is visible  ${CreateSigCertPwdField}
   choose file  ${CreateSigFileToSignField}  ${EXECDIR}\\TestData\\test_txt.txt
   choose file  ${CreateSigCertfileField}  ${SignCertPath}
   put text into field  ${SignCertPwd}  ${CreateSigCertPwdField}  ${CreateSigChooseDataElement}
   click element  ${CreateSigAddToBlockChainSwitch}
   click button  ${CreateSigBtnSign}
   wait until element is visible  ${CreateSignBtnCreateAnotherSignature}
   click button  ${CreateSignBtnSaveSignedFile}
   wait until element is enabled  ${CreateSignBtnCreateAnotherSignature}

TC_01_04_01 - Postcondition
   close browser

TC_01_04_02 - Create Xades
   Open Chrome And Login Test User
   click link  ${UserHomeBtnCreateSignature}
   wait until element is visible  ${CreateSigElement}
   click element  ${CreateSigXadesProfile}
   click button  ${CreateSigBtnContinue}
   wait until element is visible  ${CreateSigCertPwdField}
   choose file  ${CreateSigFileToSignField}  ${EXECDIR}\\TestData\\test_xml.xml
   choose file  ${CreateSigCertfileField}  ${SignCertPath}
   put text into field  ${SignCertPwd}  ${CreateSigCertPwdField}  ${CreateSigChooseDataElement}
   click element  ${CreateSigAddToBlockChainSwitch}
   click button  ${CreateSigBtnSign}
   wait until element is visible  ${CreateSignBtnCreateAnotherSignature}
   click button  ${CreateSignBtnSaveSignedFile}
   wait until element is enabled  ${CreateSignBtnCreateAnotherSignature}

TC_01_04_02 - Postcondition
   close browser

TC_01_04_03 - Create Pades
   Open Chrome And Login Test User
   click link  ${UserHomeBtnCreateSignature}
   wait until element is visible  ${CreateSigElement}
   click element  ${CreateSigPadesProfile}
   click button  ${CreateSigBtnContinue}
   wait until element is visible  ${CreateSigCertPwdField}
   choose file  ${CreateSigFileToSignField}  ${EXECDIR}\\TestData\\test_pdf.pdf
   choose file  ${CreateSigCertfileField}  ${SignCertPath}
   put text into field  ${SignCertPwd}  ${CreateSigCertPwdField}  ${CreateSigChooseDataElement}
   click element  ${CreateSigAddToBlockChainSwitch}
   click button  ${CreateSigBtnSign}
   wait until element is visible  ${CreateSignBtnCreateAnotherSignature}
   click button  ${CreateSignBtnSaveSignedFile}
   wait until element is enabled  ${CreateSignBtnCreateAnotherSignature}

TC_01_04_03 - Postcondition
   close browser

TC_01_04_04 - Create Signature No File To Sign
   Open Chrome And Login Test User
   click link  ${UserHomeBtnCreateSignature}
   wait until element is visible  ${CreateSigElement}
   click element  ${CreateSigPadesProfile}
   click button  ${CreateSigBtnContinue}
   wait until element is visible  ${CreateSigCertPwdField}
   choose file  ${CreateSigCertfileField}  ${SignCertPath}
   put text into field  ${SignCertPwd}  ${CreateSigCertPwdField}  ${CreateSigChooseDataElement}
   element should be disabled  ${CreateSigBtnSign}

TC_01_04_04 - Postcondition
   close browser

TC_01_04_05 - Create Signature No Certificate
   Open Chrome And Login Test User
   click link  ${UserHomeBtnCreateSignature}
   wait until element is visible  ${CreateSigElement}
   click element  ${CreateSigPadesProfile}
   click button  ${CreateSigBtnContinue}
   wait until element is visible  ${CreateSigCertPwdField}
   choose file  ${CreateSigFileToSignField}  ${EXECDIR}\\TestData\\test_pdf.pdf
   put text into field  ${SignCertPwd}  ${CreateSigCertPwdField}  ${CreateSigChooseDataElement}
   element should be disabled  ${CreateSigBtnSign}

TC_01_04_05 - Postcondition
   close browser

TC_01_04_06 - Create Signature No Pwd
   Open Chrome And Login Test User
   click link  ${UserHomeBtnCreateSignature}
   wait until element is visible  ${CreateSigElement}
   click element  ${CreateSigPadesProfile}
   click button  ${CreateSigBtnContinue}
   wait until element is visible  ${CreateSigCertPwdField}
   choose file  ${CreateSigFileToSignField}  ${EXECDIR}\\TestData\\test_pdf.pdf
   choose file  ${CreateSigCertfileField}  ${SignCertPath}
   element should be disabled  ${CreateSigBtnSign}

TC_01_04_06 - Postcondition
   close browser

TC_01_04_07 - Wrong cert pwd
   Open Chrome And Login Test User
   click link  ${UserHomeBtnCreateSignature}
   wait until element is visible  ${CreateSigElement}
   click element  ${CreateSigCadesProfile}
   click button  ${CreateSigBtnContinue}
   wait until element is visible  ${CreateSigCertPwdField}
   choose file  ${CreateSigFileToSignField}  ${EXECDIR}\\TestData\\test_txt.txt
   choose file  ${CreateSigCertfileField}  ${SignCertPath}
   put text into field  wrongPwdMate  ${CreateSigCertPwdField}  ${CreateSigChooseDataElement}
   click element  ${CreateSigAddToBlockChainSwitch}
   click button  ${CreateSigBtnSign}
   wait until element is visible  ${CreateSignWrongPwdError}

TC_01_04_07 - Postcondition
   close browser

TC_01_04_08 - Create Pades Add Record To Blockchain
   open chrome and login test user
   click link  ${UserHomeBtnCreateSignature}
   wait until element is visible  ${CreateSigElement}
   click element  ${CreateSigPadesProfile}
   click button  ${CreateSigBtnContinue}
   wait until element is visible  ${CreateSigCertPwdField}
   choose file  ${CreateSigFileToSignField}  ${EXECDIR}\\TestData\\test_pdf.pdf
   choose file  ${CreateSigCertfileField}  ${SignCertPath}
   put text into field  ${SignCertPwd}  ${CreateSigCertPwdField}  ${CreateSigChooseDataElement}
   click button  ${CreateSigBtnSign}
   wait until element is visible  ${CreateSignBtnCreateAnotherSignature}
   Element Should Be Visible  ${CreateSignAddedToBlockchain}
   click button  ${CreateSignBtnSaveSignedFile}
   wait until element is enabled  ${CreateSignBtnCreateAnotherSignature}

TC_01_04_08 - Postcondition
   close browser

TC_01_04_09 - Add External Cades Add To Blockchain
    Open Chrome And Login Test User
    click link  ${UserHomeBtnCreateSignature}
    wait until element is visible  ${CreateSigElement}
    click element  ${CreateSigCadesProfile}
    click button  ${CreateSigBtnContinue}
    wait until element is visible  ${CreateSigCertPwdField}
    choose file  ${CreateSigFileToSignField}  ${EXECDIR}\\TestData\\test_txt.txt
    choose file  ${CreateSigCertfileField}  ${SignCertPath}
    put text into field  ${SignCertPwd}  ${CreateSigCertPwdField}  ${CreateSigChooseDataElement}
    click button  ${CreateSigBtnSign}
    wait until element is visible  ${CreateSignBtnCreateAnotherSignature}
    element should be visible  ${CreateSignAddedToBlockchain}
    click button  ${CreateSignBtnSaveSignedFile}
    wait until element is enabled  ${CreateSignBtnCreateAnotherSignature}

TC_01_04_09 - Postcondition
    close browser
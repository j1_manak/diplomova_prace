*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing
Documentation  Suite description
Resource  Elements.robot
Resource  Buttons.robot
Resource  InputFields.robot
Resource  Browser_links.robot
Resource  Keywords.robot
Resource  CustomVariables.robot

*** Test Cases ***
TC_01_05_01 - Verify Cades
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_txt_signed.txt
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignSignatureSuccess}

TC_01_05_01 - Postcondition
    close browser

TC_01_05_02 - Verify Xades
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_xml_signed.xml
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignSignatureSuccess}

TC_01_05_02 - Postcondition
    close browser

TC_01_05_03 - Verify Pades
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_pdf_signed.pdf
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignSignatureSuccess}

TC_01_05_03 - Postcondtition
    close browser

TC_01_05_04 - Verify Signature No Signed File
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_pdf.pdf
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignNoSignature}

TC_01_05_04 - Postcondition
    close browser

TC_01_05_05 - Verify Cades External
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_txt.txt
    choose file  ${VerifySignatureExternalSignatureField}  ${EXECDIR}\\TestData\\test_txt_signature.p7s
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignSignatureSuccess}

TC_01_05_05 - Postcondition
    close browser

TC_01_05_06 - VerifyXades External
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_xml.xml
    choose file  ${VerifySignatureExternalSignatureField}  ${EXECDIR}\\TestData\\test_xml_signature.xml
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignSignatureSuccess}

TC_01_05_06 - Postcondition
    close browser

TC_01_05_07 - Verify Pades And Check In Blockchain
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_pdf_signed_bc.pdf
    click element  ${VerifySignatureCheckBlockchain}
    choose file  ${VerifySignatureChooseCert}   ${SignCertPath}
    put text into field  ${SignCertPwd}  ${VerifySignatureCertPwd}  ${VerifySigSignedDataElement}
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignSignatureSuccess}
    element should be visible  ${VerifySignBlockchainVerifyRes}

TC_01_05_07 - postcondition
    close browser

TC_01_05_08 - Verify Pades Check In Blockchain Not Find Record
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_pdf_signed.pdf
    click element  ${VerifySignatureCheckBlockchain}
    choose file  ${VerifySignatureChooseCert}   ${SignCertPath}
    put text into field  ${SignCertPwd}  ${VerifySignatureCertPwd}  ${VerifySigSignedDataElement}
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignPadesResultIsVisible}
    element should be visible  ${VerifySignNotFoundInBlockchain}

TC_01_05_08 - postcondition
    close browser

TC_01_05_09 - Verify Cades External And Check In Blockchain
    open chrome and login test user
    click link  ${UserHomeBtnVerifySignature}
    wait until element is visible  ${VerifySigSignedDataElement}
    choose file  ${VerifySignatureSignedDataField}  ${EXECDIR}\\TestData\\test_txt.txt
    choose file  ${VerifySignatureExternalSignatureField}  ${EXECDIR}\\TestData\\test_txt_signature_bc.p7s
    click element  ${VerifySignatureCheckBlockchain}
    choose file  ${VerifySignatureChooseCert}   ${SignCertPath}
    put text into field  ${SignCertPwd}  ${VerifySignatureCertPwd}  ${VerifySigSignedDataElement}
    click button  ${VerifySigBtnVerify}
    wait until element is visible  ${VerifySignPadesResultIsVisible}
    element should be visible  ${VerifySignBlockchainVerifyRes}

TC_01_05_09 - postcondition
    close browser
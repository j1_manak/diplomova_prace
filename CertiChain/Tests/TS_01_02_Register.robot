*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing
Documentation    Suite description
Resource  Elements.robot
Resource  Buttons.robot
Resource  InputFields.robot
Resource  Browser_links.robot
Resource  Keywords.robot

*** Test Cases ***
TC_01_02_01 - Register Successfully
    Open Chrome Page Go Register
    Put Text Into Field  qweasdycx@asdsad.cz  ${RegisterEmailField}  ${RegisterTitleElement}
    Put Text Into Field  Heslo1234  ${RegisterPwdField}  ${RegisterTitleElement}
    Put Text Into Field  Heslo1234  ${RegisterPwdAgainField}  ${RegisterTitleElement}
    click button  ${BtnRegister}
    wait until element is visible  ${UserHomeElement}  120

TC_01_02_01 - Postcondition
    close browser

TC_01_02_02 - Not Filled Form
    Open Chrome Page Go Register
    click button  ${BtnRegister}
    wait until element is visible  ${RegisterEmailRequiredElement}
    wait until element is visible  ${RegisterPwdRequiredElement}
    wait until element is visible  ${RegisterPwdAgainRequired}

TC_01_02_02 - Postcondition
    close browser

TC_01_02_03 - Weak Password
    Open Chrome Page Go Register
    Put Text Into Field  test3@email.cz  ${RegisterEmailField}  ${RegisterTitleElement}
    Put Text Into Field  123  ${RegisterPwdField}  ${RegisterTitleElement}
    Put Text Into Field  123  ${RegisterPwdAgainField}  ${RegisterTitleElement}
    click button  ${BtnRegister}
    wait until element is visible  ${RegisterWeakPwd}

TC_01_02_03 - PostCondition
    close browser

TC_01_02_04 - Repeat Password Badly
    Open Chrome Page Go Register
    Put Text Into Field  test3@email.cz  ${RegisterEmailField}  ${RegisterTitleElement}
    Put Text Into Field  Heslo1234  ${RegisterPwdField}  ${RegisterTitleElement}
    Put Text Into Field  BadTypedPassword  ${RegisterPwdAgainField}  ${RegisterTitleElement}
    click button  ${BtnRegister}
    wait until element is visible  ${RegisterPwdAgainDontMatch}

TC_01_02_04 - Postcondition
    close browser

TC_01_02_05 - Email Already Registered
    Open Chrome Page Go Register
    Put Text Into Field  kubish1998@email.cz  ${RegisterEmailField}  ${RegisterTitleElement}
    Put Text Into Field  Heslo1234  ${RegisterPwdField}  ${RegisterTitleElement}
    Put Text Into Field  Heslo1234  ${RegisterPwdAgainField}  ${RegisterTitleElement}
    click button  ${BtnRegister}
    wait until element is visible  ${RegisterEmailAlreadyRegistered}

TC_01_02_05 - Postcondition
    close browser
*** Variables ***
${NavBarLogin}  //a[@id='navLogin']
${NavBarRegister}  //a[@id='navRegister']

${BtnRegister}  //button[@id='btnRegister']

${BtnLogin}  //button[@id='btnLogin']
${BtnForgotPwd}  //button[@id='btnForgotPwd']
${LoginBtnResetPwd}  //button[@id='btnResetPwd']
${LoginBtnResetCancel}  //button[@id='btnResetPwdCancel']

${CreateSigBtnContinue}  //button[@id='btnContinue']
${CreateSigBtnChooseFilesGoBack}  //button[@id='0HMG614QV2E7A']
${CreateSigBtnSign}  //button[@id='btnSign']
${CreateSignBtnSaveSignedFile}  //button[@id='btnSave']
${CreateSignBtnCreateAnotherSignature}  //button[@id='btnCreateAnotherSign']

${VerifySigBtnVerify}  //button[@id='btnVerifySig']
${VerifySigBtnVerifyAnother}  //button[@id='btnVerifyAnother']

${ProfileManagementBtnCreate}  //a[@id='btnCreateProfile']

${CreateSigProfBtnCancel}  //button[@id='btnCancel']
${CreateSigProfBtnSave}  //button[@id='btnSave']
${CreateSigProfBtnNext}  //button[@id='btnNext']
${CreateSignProfBtnBack}  //button[@id='btnBack']
${CreateSignProfBtnCancelYes}  //button[@id='btnCancelYes']
${CreateSignProfBtnCancelNo}  //button[@id='btnCancelNo']

${UserSettingsBtnChangePwd}  //button[@id='btnChangePwd']
${UserSettingsBtnDeactivate}  //button[@id='btnDeactivateAccount']
${UserSettingsBtnDeactivateYes}  //button[@id='btnDeactivateYes']
${UserSettingsBtnDeactivateNo}  //button[@id='btnDeactivateNo']

${BlockchainSummaryCheckItemBtn}  //button[@id='btnVerify4']
${BlockchainSummaryFilterBtn}  //input[@id='deFilterTo']
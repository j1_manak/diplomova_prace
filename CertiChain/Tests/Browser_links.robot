*** Variables ***
${LocalhostUrl}  https://localhost:7239
${RealUrl}  https://certichain.duckdns.org

${LinkUserHome}  //a[@id='navUserHome']
${LinkUserLogout}  //a[@id='navLogout']

${UserHomeBtnCreateSignature}  //a[@id='btnGoCreateSig']
${UserHomeBtnVerifySignature}  //a[@id='btnGoVerifySig']
${UserHomeBtnSignaturesManagenent}  //a[@id='btnGoManageProfiles']
${UserHomeBtnBlockChainSummary}  //a[@id='btnGoBlockchainMan']
${UserHomeBtnAccountSettings}  //a[@id='btnGoAccountSettings']

${CreateSigBtnCreateProfile}  //a[@id='btnCreateProfile']

${VerifySignatureGoUserHome}  //a[@id='btnBackHome']

${ProfileManagementGoCreateProfile}  //a[@id='0HMG614QV2E85']
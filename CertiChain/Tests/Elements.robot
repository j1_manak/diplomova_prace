*** Variables ***
${WelcomeScreenElement}  //p[contains(text(),'Proč používat aplikaci CertiChain?')]

${LoginBadEmailPwd}  //strong[contains(text(),'Uživatel nebyl nalezen nebo bylo špatně zadáno hes')]
${LoginTitle}  //h2[contains(text(),'Přihlášení')]

${RegisterTitleElement}  //h2[contains(text(),'Registrace')]
${RegisterPwdRequiredElement}  //div[contains(text(),'Heslo musí být vyplněno')]
${RegisterEmailRequiredElement}  //div[contains(text(),'Email musí být vyplněn')]
${RegisterPwdAgainRequired}  //div[contains(text(),'Email musí být vyplněn')]
${RegisterWeakPwd}  //div[contains(text(),'Heslo musí mít alespoň 8 znaků')]
${RegisterPwdAgainDontMatch}  //div[contains(text(),'Hesla ne neshodují')]
${RegisterEmailAlreadyRegistered}  //strong[contains(text(),'Pro zadaný email již existuje uživatelský účet.')]

${UserHomeElement}  //h2[contains(text(),'Vytvoření podpisu')]

${CreateSigProfProfName}  //label[contains(text(),'Název profilu')]
${CreateSigPadesType}  //label[contains(text(),'Typ podpisu PAdES')]
${CreateSigPadesVisible}  //h4[contains(text(),'Výběr umístění viditelného podpisu')]

${CreateSigCadesProfile}  //h4[contains(text(),'Test-cades')]
${CreateSigPadesProfile}  //h4[contains(text(),'Test-Pades-Invisibled')]
${CreateSigXadesProfile}  //h4[contains(text(),'Test-Xades')]
${CreateSigElement}  //span[contains(text(),'Výběr profilu')]
${CreateSigChooseDataElement}  //label[contains(text(),'Soubor k podepsání')]
${CreateSignWrongPwdError}  //p[contains(text(),'Při vytváření podpisu nastala chyba - Heslo k pode')]
${CreateSignAddedToBlockchain}  //strong[contains(text(),'Podpis byl úspešně uložen do blockchainového systé')]

${VerifySigSignedDataElement}  //label[contains(text(),'Podepsaný soubor (data ke kterým byl vytvořen exte')]
${VerifySignSignatureSuccess}  //label[contains(text(),'Čas ověření podpisu')]
${VerifySignNoSignature}  //strong[contains(text(),'V ověřovaném dokumentu nebyl nalezen žádný digitál')]
${VerifySignBlockchainVerifyRes}  //h6[contains(text(),'Záznam je validní. Ověřovaná data byla podepsána c')]
${VerifySignNotFoundInBlockchain}  //strong[contains(text(),'Nebyl nalezen žádný záznam v systému blockchain pr')]
${VerifySignPadesResultIsVisible}  //label[contains(text(),'Autor podpisu')]

${BlockchainSummaryTitleElement}  //h3[contains(text(),'Přehled záznamů uživatele v systému Blockchain')]
${BlockchainSummaryFirstItem}  //th[contains(text(),'07791918-461c-411a-aa5c-8a31a81d42a4')]
${BlockchainSummaryVerifySuccess}  //h5[contains(text(),'Úspěch')]
${BlockchainSummaryFilterElementResult}  //th[contains(text(),'1049777c-a473-45a4-8f98-155f7ed7b201')]
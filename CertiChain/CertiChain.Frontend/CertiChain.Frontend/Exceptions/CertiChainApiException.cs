﻿using CertiChain.Frontend.JsonObjects.Error;

namespace CertiChain.Frontend.Exceptions
{
    public class CertiChainApiException : Exception
    {
        public CertiChainApiException() 
        {

        }

        public CertiChainApiException(ErrorResp errorDetails)
        {
            ErrorDetails = errorDetails;
        }

        public ErrorResp ErrorDetails { get; private set; }
    }
}

using Blazored.LocalStorage;
using Blazored.Toast;
using Blazored.Toast.Services;
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using CertiChain.Frontend;
using CertiChain.Frontend.AppServices.Account;
using CertiChain.Frontend.AppServices.Profiles;
using CertiChain.Frontend.Auth;
using CertiChain.Frontend.Utils;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.JSInterop;
using System.Net.Http.Headers;
using CertiChain.Frontend.AppServices.Blockchain;
using CertiChain.Frontend.AppServices.Signature;

var builder = WebAssemblyHostBuilder.CreateDefault(args);

builder.Services
     .AddBlazorise(options =>
     {
         
     })
     .AddBootstrapProviders()
     .AddFontAwesomeIcons()
     .AddBlazoredToast();

builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped<AuthenticationStateProvider, CustomAuthenticationProvider>();
builder.Services.AddAuthorizationCore();
builder.Services.AddBlazoredLocalStorage();


builder.Services.AddScoped<IAccountService, AccountService>();
builder.Services.AddScoped<IProfileService, ProfileService>();
builder.Services.AddScoped<ISignatureService, SignatureService>();
builder.Services.AddScoped<IBlockchainService, BlockchainService>();

builder.Services.AddScoped(sp =>
{
    var clientHandler = new HttpClientHandler()
    {
        
    };

    var localUrl = "http://localhost:8080/certichain/api/v1/";
    var mainUrl = "https://certichain.duckdns.org:8080/certichain/api/v1/";
    
    var client = new CustomHttpClient(clientHandler, sp.GetService<NavigationManager>(), sp.GetService<ILocalStorageService>(),
        sp.GetService<IToastService>())
    {
        BaseAddress = new Uri(mainUrl),
        Timeout = TimeSpan.FromSeconds(100)
    };

    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(CertiChainApiConstants.JsonMediaType));

    return client;
});

await builder.Build().RunAsync();

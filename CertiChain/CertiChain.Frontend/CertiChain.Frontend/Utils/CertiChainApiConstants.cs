﻿namespace CertiChain.Frontend.Utils
{
    public static class CertiChainApiConstants
    {
        public const string JsonMediaType = "application/json";
        public const string BearerScheme = "Bearer";

        // nedávat lomítko na začátek uri
        public const string UserLoginUri = "user/login";
        public const string UserRegisterUri = "user/register";
        public const string UserForgotPwdUri = "user/forgottenPassword";
        public const string UserChangePwdUri = "user/changePassword";
        public const string UserDeactivateAccountUri = "user/delete";
        public const string UserResendActivationEmailUri = "user/resendActivationEmail";
        public const string UserGetInfoUri = "user/info";

        public const string ProfileGetAll = "profiles";
        public const string ProfileAddSuffix = "add";

        public const string SignatureCreateUri = "signature/create";
        public const string SignatureVerifyUri = "signature/verify";

        public const string BlockchainGetUserRecordsUri = "blockchain/getUserRecords";
        public const string BlockchainVerifyRecordUri = "blockchain/verifyRecord";
    }
}

﻿namespace CertiChain.Frontend.Utils;

public static class FrontendUris
{
    public const string UserHome = "/user/home";
    public const string UserLogin = "/user/login";
    public const string UserRegister = "/user/register";
    public const string UserSettings = "/user/settings";
    public const string UserVerifyEmail = "/user/confirmEmail/success";
    
    public const string ProfileEdit = "/profiles/edit/";
    public const string ProfileManagement = "/profiles/management";
    public const string ProfileCreateNew = "/profiles/edit/new";

    public const string SignatureCreate = "/signature/create";
    public const string SignatureVerify = "/signature/verify";
    
    public const string BlockchainSummary = "/blockchain/summary";
}
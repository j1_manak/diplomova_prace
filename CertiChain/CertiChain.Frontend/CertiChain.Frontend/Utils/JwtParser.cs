﻿using System.Security.Claims;
using System.Text.Json;

namespace CertiChain.Frontend.Utils
{
    public static class JwtParser
    {
        public const string JwtTokenKeyName = "token";
        
        public static List<Claim> ParseClaimsFromJwt(string jwt)
        {
            var claims = new List<Claim>();
            var payload = jwt.Split('.')[1];

            var jsonBytes = ParseBase64WithoutPadding(payload);

            var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);

            claims.AddRange(keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString())));

            return claims;
        }

        public static string GetUserIdFromToken(string jwt)
        {
            var claims = ParseClaimsFromJwt(jwt);
            var idClaim = claims.Find(x => x.Type == "id");
            return idClaim?.Value;
        }

        private static byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }
            return Convert.FromBase64String(base64);
        }
    }
}

﻿using Blazored.LocalStorage;
using Blazored.Toast.Services;
using CertiChain.Frontend.AppServices.Account;
using CertiChain.Frontend.Exceptions;
using CertiChain.Frontend.JsonObjects.Error;
using Microsoft.AspNetCore.Components;
using System.Net;
using System.Text.Json;

namespace CertiChain.Frontend.Utils
{
    public class CustomHttpClient : HttpClient
    {
        private readonly NavigationManager _navigationManager;
        private readonly ILocalStorageService _localStorageService;
        private readonly IToastService _toastService;

        public CustomHttpClient(NavigationManager navigationManager, ILocalStorageService localStorageService, IToastService toastService)
        {
            _navigationManager = navigationManager;
            _localStorageService = localStorageService;
            _toastService = toastService;
        }

        public CustomHttpClient(HttpMessageHandler handler, NavigationManager navigationManager, ILocalStorageService localStorageService,
            IToastService toastService) : base(handler)
        {
            _navigationManager = navigationManager;
            _localStorageService = localStorageService;
            _toastService = toastService;
        }

        public new async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            Console.WriteLine("CustomClient - SendAsync");
            var response = await base.SendAsync(request);
            if(response.StatusCode == HttpStatusCode.Unauthorized)
            {
                Console.WriteLine("CustomClient - server returned status 403 or 401");
                // logout from
                _navigationManager.NavigateTo("/user/login", true);
                await _localStorageService.RemoveItemAsync("token");
                _toastService.ShowInfo("Vaše relase vypršela a je potřeba se znova přihlásit", "Informace");
            }
            return response;
        }

        public async Task ParseErrorResp(HttpResponseMessage response)
        {
            var respJson = await response.Content.ReadAsStringAsync();
            Console.WriteLine(respJson);
            if (!string.IsNullOrWhiteSpace(respJson))
            {
                var errorDetails = JsonSerializer.Deserialize<ErrorResp>(respJson);
                throw new CertiChainApiException(errorDetails);
            }
            else throw new Exception($"{(int)response.StatusCode} - {response.ReasonPhrase}");
        }
    }
}

﻿namespace CertiChain.Frontend.Utils
{
    public static class CustomColors
    {
        public const string Black = "#000000";
        public const string White = "#FFFFFF";
        public const string PrimaryOrange = "#e98214";
        public const string SecondaryOrange = "#A68E02";
        public const string Secondary = "#88A5AE";
        public const string PrimaryBackground = "#F5F5F5";
        public const string Danger = "#C3683E";
        public const string Dark = "#130c48";
        public const string WhiteSmoke = "#F5F5F5";
        public const string Success = "#55a608";
        public const string Info = "#407bb8";
        public const string Warning = "#ffc05c";
        public const string Light = "#F5F5F5";
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace CertiChain.Frontend.Utils;

public static class EnumUtils
{
    public static string GetDisplayName(this Enum enumValue) => enumValue.GetType()
        .GetMember(enumValue.ToString())
        .First()
        .GetCustomAttribute<DisplayAttribute>()
        .GetName();
}
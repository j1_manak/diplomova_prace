﻿using Blazored.LocalStorage;
using CertiChain.Frontend.Auth;
using CertiChain.Frontend.Exceptions;
using CertiChain.Frontend.JsonObjects.Error;
using CertiChain.Frontend.JsonObjects.Login;
using CertiChain.Frontend.JsonObjects.User;
using CertiChain.Frontend.Models.Account;
using CertiChain.Frontend.Utils;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace CertiChain.Frontend.AppServices.Account
{
    public class AccountService : IAccountService
    {
        private readonly AuthenticationStateProvider _authStateProv;
        private readonly ILocalStorageService _localStorage;
        private readonly CustomHttpClient _httpClient;

        private UserInfoResp _userInfo;

        public AccountService(AuthenticationStateProvider authStateProv, ILocalStorageService localStorage, CustomHttpClient httpClient)
        {
            _authStateProv = authStateProv;
            _localStorage = localStorage;
            _httpClient = httpClient;
        }

        #region PublicMethods

        public async Task<bool> LoginAsync(LoginModel model)
        {
            var reqObject = new LoginRegisterReq(model?.Email, model?.Password);
            return await CommunicateWithApi(CertiChainApiConstants.UserLoginUri, reqObject);
        }

        public async Task<bool> LogoutAsync()
        {
            await _localStorage.RemoveItemAsync(JwtParser.JwtTokenKeyName);
            _httpClient.DefaultRequestHeaders.Authorization = null;
            var customAuthProv = _authStateProv as CustomAuthenticationProvider;
            if (customAuthProv is not null)
            {
                customAuthProv.Notify();
                return true;
            }
            else return false;
        }

        public async Task<bool> RegisterAsync(RegisterModel registerModel)
        {
            var reqObject = new LoginRegisterReq(registerModel?.Email, registerModel?.PasswordAgain);
            return await CommunicateWithApi(CertiChainApiConstants.UserRegisterUri, reqObject);
        }

        public async Task<bool> ResetPasswordAsync(ResetPwdModel model)
        {
            var request = new HttpRequestMessage(HttpMethod.Put, CertiChainApiConstants.UserForgotPwdUri);

            var forgotPwdJson = JsonSerializer.Serialize(model);
            request.Content = new StringContent(forgotPwdJson, Encoding.UTF8, CertiChainApiConstants.JsonMediaType);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(CertiChainApiConstants.JsonMediaType));

            var response = await _httpClient.SendAsync(request);
            var respContent = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode) return true;
            else
            {
                var errorObject = JsonSerializer.Deserialize<ErrorResp>(respContent);
                throw new CertiChainApiException(errorObject);
            }
        }

        public async Task<string> GetUsernameAsync()
        {
            var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (!string.IsNullOrWhiteSpace(token))
            {
                var claims = JwtParser.ParseClaimsFromJwt(token);
                var username = claims.FirstOrDefault(x => x.Type == "username")?.Value;
                return username;
            }
            else return null;
        }

        public async Task<bool> GetIsLoggedInAsync()
        {
            var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            return string.IsNullOrWhiteSpace(token) ? false : true;
        }

        public async Task<bool> ChangePasswordAsync(ChangePwdModel model)
        {
            var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            var claims = JwtParser.ParseClaimsFromJwt(token);
            var id = claims.FirstOrDefault(x => x.Type == "id")?.Value;
            // should not be null
            if (!string.IsNullOrWhiteSpace(token) && !string.IsNullOrWhiteSpace(id))
            {
                var request = new HttpRequestMessage(HttpMethod.Put, $"{CertiChainApiConstants.UserChangePwdUri}/{id}");
                var reqObj = new ChangePwdReq(model.OldPassword, model.NewPassword);
                var reqJson = JsonSerializer.Serialize(reqObj);
                request.Content = new StringContent(reqJson, Encoding.UTF8, CertiChainApiConstants.JsonMediaType);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(CertiChainApiConstants.JsonMediaType));
                request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

                var response = await _httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    await _httpClient.ParseErrorResp(response); // always throw Exception
                    return false;
                }
            }
            else throw new Exception("JWT token nenalezen");
        }

        public async Task<bool> DeactivateAccountAsync(string pwd)
        {
            var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            var claims = JwtParser.ParseClaimsFromJwt(token);
            var username = claims.FirstOrDefault(x => x.Type == "username")?.Value;
            // should not be null
            if (!string.IsNullOrWhiteSpace(token) && !string.IsNullOrWhiteSpace(username))
            {
                var request = new HttpRequestMessage(HttpMethod.Post, CertiChainApiConstants.UserDeactivateAccountUri);
                var reqObject = new LoginRegisterReq(username, pwd);
                var reqJson = JsonSerializer.Serialize(reqObject);
                request.Content = new StringContent(reqJson, Encoding.UTF8, CertiChainApiConstants.JsonMediaType);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(CertiChainApiConstants.JsonMediaType));
                request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

                var response = await _httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode) return true;
                else
                {
                    await _httpClient.ParseErrorResp(response);
                    return false;
                }
            }
            else throw new Exception("JWT token nenalezen");
        }

        public async Task<bool> GetHasConfirmedEmailAsync()
        {
            var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (!string.IsNullOrWhiteSpace(token))
            {
                var claims = JwtParser.ParseClaimsFromJwt(token);
                var value = claims.FirstOrDefault(x => x.Type == "emailConfirmed")?.Value;
                Console.WriteLine($"Email confirmed - {value}");
                return value?.ToLower() == "false" ? false : true;
            }
            else return true;
        }

        public async Task<bool> ResendActivationEmailAsync()
        {
            var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (!string.IsNullOrWhiteSpace(token))
            {
                var claims = JwtParser.ParseClaimsFromJwt(token);
                var id = claims.FirstOrDefault(x => x.Type == "id")?.Value;
                if (!string.IsNullOrWhiteSpace(id))
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, $"{CertiChainApiConstants.UserResendActivationEmailUri}/{id}");

                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(CertiChainApiConstants.JsonMediaType));
                    request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

                    var response = await _httpClient.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                    {
                        await _httpClient.ParseErrorResp(response);
                    }
                }
            }
            return false;
        }

        public async Task<UserInfoResp> GetUserInfoAsync()
        {
            if (_userInfo != null) return _userInfo;

            var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (!string.IsNullOrWhiteSpace(token))
            {
                var claims = JwtParser.ParseClaimsFromJwt(token);
                var id = claims.FirstOrDefault(x => x.Type == "id")?.Value;
                if (!string.IsNullOrWhiteSpace(id))
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, $"{CertiChainApiConstants.UserGetInfoUri}/{id}");

                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(CertiChainApiConstants.JsonMediaType));
                    request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

                    var response = await _httpClient.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        var respJson = await response.Content.ReadAsStringAsync();
                        var userInfo = JsonSerializer.Deserialize<UserInfoResp>(respJson);
                        _userInfo = userInfo;
                        return _userInfo;
                    }
                    else
                    {
                        await _httpClient.ParseErrorResp(response);
                    }
                }
            }
            return null;
        }

        #endregion

        #region PrivateMethods

        private async Task<bool> CommunicateWithApi(string endpoint, LoginRegisterReq req)
        {
            
            var reqJson = JsonSerializer.Serialize(req);

            var request = new HttpRequestMessage(HttpMethod.Post, endpoint);
            request.Content = new StringContent(reqJson, Encoding.UTF8, CertiChainApiConstants.JsonMediaType);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(CertiChainApiConstants.JsonMediaType));

            var response = await _httpClient.SendAsync(request);
            var loginRespString = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                var respObject = JsonSerializer.Deserialize<LoginRegisterResp>(loginRespString);
                await _localStorage.SetItemAsync(JwtParser.JwtTokenKeyName, respObject?.JwtToken);
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, respObject?.JwtToken);

                var customAuthProv = _authStateProv as CustomAuthenticationProvider;
                if (customAuthProv is not null)
                {
                    customAuthProv.Notify();
                    return true;
                }
                else return false;
            }
            else
            {
                var errorObject = JsonSerializer.Deserialize<ErrorResp>(loginRespString);
                throw new CertiChainApiException(errorObject);
            }
        }

        #endregion
    }
}

﻿using CertiChain.Frontend.JsonObjects.User;
using CertiChain.Frontend.Models.Account;

namespace CertiChain.Frontend.AppServices.Account
{
    public interface IAccountService
    {
        Task<bool> LoginAsync(LoginModel model);

        Task<bool> LogoutAsync();

        Task<bool> RegisterAsync(RegisterModel model);

        Task<bool> ResetPasswordAsync(ResetPwdModel model);

        Task<bool> ChangePasswordAsync(ChangePwdModel model);

        Task<bool> DeactivateAccountAsync(string pwd);

        Task<bool> ResendActivationEmailAsync();

        Task<UserInfoResp> GetUserInfoAsync();

        Task<bool> GetIsLoggedInAsync();

        Task<string> GetUsernameAsync();

        Task<bool> GetHasConfirmedEmailAsync();
    }
}

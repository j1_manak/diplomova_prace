using CertiChain.Frontend.JsonObjects.Blockchain;
using CertiChain.Frontend.JsonObjects.Signature;

namespace CertiChain.Frontend.AppServices.Blockchain;

public interface IBlockchainService
{
    Task<GetUserBlockchainRecordsResp> GetUserRecordsAsync();

    Task<BlockchainVerifyStatus?> CheckRecordValidityAsync(string recordId);
}
using System.Net.Http.Headers;
using Blazored.LocalStorage;
using CertiChain.Frontend.JsonObjects.Blockchain;
using CertiChain.Frontend.JsonObjects.Signature;
using CertiChain.Frontend.Utils;
using Newtonsoft.Json;

namespace CertiChain.Frontend.AppServices.Blockchain;

public class BlockchainService : IBlockchainService
{
    private readonly CustomHttpClient _httpClient;
    private readonly ILocalStorageService _localStorage;

    public BlockchainService(CustomHttpClient httpClient, ILocalStorageService localStorage)
    {
        _httpClient = httpClient;
        _localStorage = localStorage;
    }

    public async Task<GetUserBlockchainRecordsResp> GetUserRecordsAsync()
    {
        var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
        if (string.IsNullOrWhiteSpace(token)) return null;
        
        var userId = JwtParser.GetUserIdFromToken(token);
        if (string.IsNullOrWhiteSpace(userId)) return null;
        
        var request = new HttpRequestMessage(HttpMethod.Get, $"{CertiChainApiConstants.BlockchainGetUserRecordsUri}/{userId}");
        request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

        var response = await _httpClient.SendAsync(request);
        if (response.IsSuccessStatusCode == false) await _httpClient.ParseErrorResp(response);

        return JsonConvert.DeserializeObject<GetUserBlockchainRecordsResp>(await response.Content.ReadAsStringAsync());
    }

    public async Task<BlockchainVerifyStatus?> CheckRecordValidityAsync(string recordId)
    {
        if (string.IsNullOrWhiteSpace(recordId))
            throw new Exception("Není zadáno id záznamu v blockchain");
        
        var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
        if (string.IsNullOrWhiteSpace(token)) return null;

        var request = new HttpRequestMessage(HttpMethod.Get, $"{CertiChainApiConstants.BlockchainVerifyRecordUri}/{recordId}");
        request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

        var response = await _httpClient.SendAsync(request);
        if (response.IsSuccessStatusCode == false) await _httpClient.ParseErrorResp(response);

        var respObj = JsonConvert.DeserializeObject<VerifyRecordResp>(await response.Content.ReadAsStringAsync());
        return respObj?.VerifyStatus;
    }
}
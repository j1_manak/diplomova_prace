﻿using CertiChain.Frontend.JsonObjects.Profiles;
using CertiChain.Frontend.Models.Profiles;

namespace CertiChain.Frontend.AppServices.Profiles
{
    public interface IProfileService
    {
        Task<List<ProfileGeneralModel>> GetProfilesAsync();

        Task<ProfileGeneralReqResp> GetFullProfileAsync(long id);

        Task<bool> AddProfileAsync(ProfileGeneralReqResp model);

        Task<bool> UpdateProfileAsync(ProfileGeneralReqResp model);

        Task<bool> DeleteProfileAsync(long id);
    }
}

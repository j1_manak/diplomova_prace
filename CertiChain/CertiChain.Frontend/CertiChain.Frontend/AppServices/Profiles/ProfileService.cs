﻿using Blazored.LocalStorage;
using CertiChain.Frontend.AppServices.Account;
using CertiChain.Frontend.JsonObjects.Profiles;
using CertiChain.Frontend.Utils;
using System.Net.Http.Headers;
using System.Text;
using CertiChain.Frontend.Models.Profiles;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;

namespace CertiChain.Frontend.AppServices.Profiles
{
    public class ProfileService : IProfileService
    {
        private readonly IAccountService _accountService;
        private readonly CustomHttpClient _httpClient;
        private readonly ILocalStorageService _localStorageService;


        public ProfileService(IAccountService accountService, CustomHttpClient httpClient, ILocalStorageService localStorageService)
        {
            _accountService = accountService;
            _httpClient = httpClient;
            _localStorageService = localStorageService;
        }

        

        public async Task<bool> AddProfileAsync(ProfileGeneralReqResp model)
        {
            var token = await _localStorageService.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (!string.IsNullOrWhiteSpace(token))
            {
                var userId = await _accountService.GetUserInfoAsync();
                var request = new HttpRequestMessage(HttpMethod.Post, $"{CertiChainApiConstants.ProfileGetAll}/{userId.Id}/{CertiChainApiConstants.ProfileAddSuffix}");
                request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);
                var jsonReq = JsonConvert.SerializeObject(model);
                request.Content = new StringContent(jsonReq, Encoding.UTF8, CertiChainApiConstants.JsonMediaType);

                var response = await _httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode) return true;
                else await _httpClient.ParseErrorResp(response); //  will throw exception

                return false;
            }
            else
            {
                Console.WriteLine("token is null");
                return false;
            }
        }

        public async Task<bool> DeleteProfileAsync(long id)
        {
            var token = await _localStorageService.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (!string.IsNullOrWhiteSpace(token))
            {
                var userId = await _accountService.GetUserInfoAsync();

                var request = new HttpRequestMessage(HttpMethod.Delete,
                    $"{CertiChainApiConstants.ProfileGetAll}/{userId.Id}/{id}");
                request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

                var response = await _httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode == false)
                {
                    await _httpClient.ParseErrorResp(response);
                }

                return true;
            }
            else return false;
        }

        public async Task<ProfileGeneralReqResp> GetFullProfileAsync(long id)
        {
            var token = await _localStorageService.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (!string.IsNullOrWhiteSpace(token))
            {
                var userId = await _accountService.GetUserInfoAsync();

                var request = new HttpRequestMessage(HttpMethod.Get,
                    $"{CertiChainApiConstants.ProfileGetAll}/{userId.Id}/{id}");
                request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

                var response = await _httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(content);
                    var profile = JsonConvert.DeserializeObject<ProfileGeneralReqResp>(content);
                    return profile;
                }
                else await _httpClient.ParseErrorResp(response);
            }

            return null;
        }

       

        public async Task<List<ProfileGeneralModel>> GetProfilesAsync()
        {
            var userInfo = await _accountService.GetUserInfoAsync();
            var token = await _localStorageService.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (userInfo?.Id is not null && !string.IsNullOrWhiteSpace(token))
            {
                var request = new HttpRequestMessage(HttpMethod.Get, $"{CertiChainApiConstants.ProfileGetAll}/{userInfo.Id}");
                request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);
                
                var response = await _httpClient.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Console.WriteLine($"GetProfilesAsync returned json - {content}");


                    var profiles = JsonConvert.DeserializeObject<List<ProfileGeneralReqResp>>(content);
                    var parsedProfiles = new List<ProfileGeneralModel>();
                    foreach (var profile in profiles)
                    {
                        var profileModel = new ProfileGeneralModel()
                        {
                            Id = profile.Id,
                            Name = profile.Name,
                            HashAlgorithm = profile.HashAlgorithm,
                            ProfileType = profile.ProfileType
                        };
                        parsedProfiles.Add(profileModel);
                    }

                    return parsedProfiles;
                }
                else
                {
                    await _httpClient.ParseErrorResp(response);
                    return null;
                }
            }
            else throw new ArgumentNullException("Není dostupné id přihlášeného uživatele");
        }

        public async Task<bool> UpdateProfileAsync(ProfileGeneralReqResp model)
        {
            var token = await _localStorageService.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (!string.IsNullOrWhiteSpace(token))
            {
                var userId = await _accountService.GetUserInfoAsync();
                var request = new HttpRequestMessage(HttpMethod.Put, $"{CertiChainApiConstants.ProfileGetAll}/{userId.Id}/{model.Id}");
                request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);
                var jsonReq = JsonConvert.SerializeObject(model);
                Console.WriteLine(jsonReq);
                request.Content = new StringContent(jsonReq, Encoding.UTF8, CertiChainApiConstants.JsonMediaType);

                var response = await _httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode) return true;
                
                await _httpClient.ParseErrorResp(response); //  will throw exception
                return false;
            }
            else
            {
                Console.WriteLine("token is null");
                return false;
            }
        }
    }
}

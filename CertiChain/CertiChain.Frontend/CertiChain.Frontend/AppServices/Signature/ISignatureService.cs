﻿using CertiChain.Frontend.JsonObjects.Signature;
using CertiChain.Frontend.Models.Signature;

namespace CertiChain.Frontend.AppServices.Signature;

public interface ISignatureService
{
    Task<CreateSignatureResp> CreateSignatureAsync(CreateSignatureReq createSignatureReq);

    Task<VerifySignatureResp> VerifySignatureAsync(VerifySignatureModel verifySignatureModel);
}
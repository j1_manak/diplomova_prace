﻿using System.Net.Http.Headers;
using System.Text;
using Blazored.LocalStorage;
using CertiChain.Frontend.AppServices.Account;
using CertiChain.Frontend.JsonObjects.Signature;
using CertiChain.Frontend.Models.Signature;
using CertiChain.Frontend.Utils;
using Newtonsoft.Json;

namespace CertiChain.Frontend.AppServices.Signature;

public class SignatureService : ISignatureService
{
    private readonly CustomHttpClient _httpClient;
    private readonly ILocalStorageService _localStorage;
    private readonly IAccountService _accountService;

    public SignatureService(CustomHttpClient httpClient, ILocalStorageService localStorage, IAccountService accountService)
    {
        _httpClient = httpClient;
        _localStorage = localStorage;
        _accountService = accountService;
    }

    public async Task<CreateSignatureResp> CreateSignatureAsync(CreateSignatureReq createSignatureReq)
    {
        var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
        var userId = JwtParser.GetUserIdFromToken(token);
        if (!string.IsNullOrWhiteSpace(token) && !string.IsNullOrWhiteSpace(userId))
        {
            var request = new HttpRequestMessage(HttpMethod.Post, CertiChainApiConstants.SignatureCreateUri);
            createSignatureReq.UserId = userId;
            var json = JsonConvert.SerializeObject(createSignatureReq);
            request.Content = new StringContent(json, Encoding.UTF8, CertiChainApiConstants.JsonMediaType);
            request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

            var response = await _httpClient.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var jsonResp = await response.Content.ReadAsStringAsync();
                Console.WriteLine(jsonResp);
                return JsonConvert.DeserializeObject<CreateSignatureResp>(jsonResp);
            }
            await _httpClient.ParseErrorResp(response);
        }

        return null;
    }

    public async Task<VerifySignatureResp> VerifySignatureAsync(VerifySignatureModel verifySignatureModel)
    {
        var token = await _localStorage.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
        if (!string.IsNullOrWhiteSpace(token))
        {
            var reqObj = new VerifySignatureReq();

            await using (var signedFileStream = new BufferedStream(verifySignatureModel.SignedData.OpenReadStream(long.MaxValue)))
            {
                reqObj.SignedData = new byte[signedFileStream.Length];
                await signedFileStream.ReadAsync(reqObj.SignedData, 0, (int)signedFileStream.Length);
                reqObj.SignedDataMimeType = verifySignatureModel.SignedData.Type;
                reqObj.SignedDataFileName = verifySignatureModel.SignedData.Name;
            }

            if (verifySignatureModel.ExternaSignature != null)
            {
                await using (var externalSigStream =
                             new BufferedStream(verifySignatureModel.ExternaSignature.OpenReadStream(long.MaxValue)))
                {
                    reqObj.ExternalSignature = new byte[externalSigStream.Length];
                    await externalSigStream.ReadAsync(reqObj.ExternalSignature, 0, (int)externalSigStream.Length);
                    reqObj.ExternalSignatureMimeType = verifySignatureModel.ExternaSignature.Type;
                    reqObj.ExternalSignatureFileName = verifySignatureModel.ExternaSignature.Name;
                }
            }

            if (verifySignatureModel.VerifyInBlockchain)
            {
                await using (var certStream =
                             new BufferedStream(verifySignatureModel.SignerCertificate.OpenReadStream()))
                {
                    reqObj.VerifyInBlockchain = verifySignatureModel.VerifyInBlockchain;
                    reqObj.SignerCertificate = new byte[certStream.Length];
                    await certStream.ReadAsync(reqObj.SignerCertificate, 0, (int)certStream.Length);
                    reqObj.SignerCertMimeType = verifySignatureModel.SignerCertificate.Type;
                }

                reqObj.SignerCertPwd = verifySignatureModel.SignerCertPwd;
            }

            var reqJson = JsonConvert.SerializeObject(reqObj);
            var request = new HttpRequestMessage(HttpMethod.Post, CertiChainApiConstants.SignatureVerifyUri);
            request.Content = new StringContent(reqJson, Encoding.UTF8, CertiChainApiConstants.JsonMediaType);
            request.Headers.Authorization = new AuthenticationHeaderValue(CertiChainApiConstants.BearerScheme, token);

            var response = await _httpClient.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var respJson = await response.Content.ReadAsStringAsync();
                Console.WriteLine(respJson);
                return JsonConvert.DeserializeObject<VerifySignatureResp>(respJson);
            }

            await _httpClient.ParseErrorResp(response);
        }
        
        return null;
    }
}
﻿using Newtonsoft.Json;

namespace CertiChain.Frontend.JsonObjects.Signature;

[JsonObject]
public class CreateSignatureReq
{
    [JsonProperty("profileId")]
    public long ProfileId { get; set; }

    [JsonProperty("userId")]
    public string UserId { get; set; }
    
    [JsonProperty("dataToSign")]
    public byte[] DataToSign { get; set; }

    [JsonProperty("fileName")]
    public string FileName { get; set; }

    [JsonProperty("fileMimeType")]
    public string FileMimeType { get; set; }

    [JsonProperty("fileExtension")]
    public string FileExtension { get; set; }
    
    [JsonProperty("signCertificate")]
    public byte[] SignCertificate { get; set; }
    
    [JsonProperty("certPwd")]
    public string CertPwd { get; set; }
    
    [JsonProperty("addToBlockchain")] 
    public bool AddToBlockChain { get; set; }
}
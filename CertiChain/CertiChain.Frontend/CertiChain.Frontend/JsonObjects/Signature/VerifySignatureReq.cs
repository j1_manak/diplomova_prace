﻿using Newtonsoft.Json;

namespace CertiChain.Frontend.JsonObjects.Signature;

[JsonObject]
public class VerifySignatureReq
{
    [JsonProperty("signedData")]
    public byte[] SignedData { get; set; }

    [JsonProperty("signedDataFileName")]
    public string SignedDataFileName { get; set; }

    [JsonProperty("signedDataMimeType")]
    public string SignedDataMimeType { get; set; }

    [JsonProperty("externalSignature")]
    public byte[] ExternalSignature { get; set; }

    [JsonProperty("externalSignatureFileName")]
    public string ExternalSignatureFileName { get; set; }

    [JsonProperty("externalSignatureMimeType")]
    public string ExternalSignatureMimeType { get; set; }
    
    [JsonProperty("verifyInBlockchain")] 
    public bool VerifyInBlockchain { get; set; }

    [JsonProperty("signerCertificate")]
    public byte[] SignerCertificate { get; set; }

    [JsonProperty("signerCertMimeType")]
    public string SignerCertMimeType { get; set; }

    [JsonProperty("signerCertPwd")]
    public string SignerCertPwd { get; set; }
    
}
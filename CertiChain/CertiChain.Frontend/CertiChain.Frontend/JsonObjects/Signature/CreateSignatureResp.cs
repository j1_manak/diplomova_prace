﻿using Newtonsoft.Json;

namespace CertiChain.Frontend.JsonObjects.Signature;

[JsonObject]
public class CreateSignatureResp
{
    [JsonProperty("signature")]
    public byte[] Signature { get; set; }
    
    [JsonProperty("signatureMimeType")]
    public string SignatureMimeType { get; set; }
    
    [JsonProperty("signatureFileExtensions")]
    public string SignatureFileExtension { get; set; }

    [JsonProperty("addedToBlockchain")]
    public bool AddedToBlockchain { get; set; }

    [JsonProperty("addToBlockchainError")]
    public string AddToBlockchainErrorMsg { get; set; }
}
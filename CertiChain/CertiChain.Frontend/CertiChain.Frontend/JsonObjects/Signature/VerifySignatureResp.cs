﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CertiChain.Frontend.JsonObjects.Signature;

[JsonObject]
public class VerifySignatureResp
{
    [JsonProperty("signatures")]
    public List<SignatureInfo> Signatures { get; set; }

    [JsonProperty("verifiedInBlockchain")]
    public bool VerifiedInBlockchain { get; set; }
    
    [JsonProperty("blockchainVerifyRes")] 
    public BlockchainVerifyRes BlockchainVerifyResult { get; set; }
}

public enum VerifyIndications
{
    [Display(Name = "Digitální podpis byl úspěšně oveřen včetně všech validačních politik")]
    TOTAL_PASSED,
    [Display(Name = "Výsledek ověření je Neurčitý. Detailnější informace jsou dostupné v podrobnostech o výsledku ověření.")]
    INDETERMINATE,
    [Display(Name = "Ověření digitálního podpisu selhalo. Důvod selhání ověření je uveden v \"Subindikaci\" výsledku ověření")]
    TOTAL_FAILED,
    [Display(Name = "V ověřovaných datech nebyl nalezen žádný digitální podpis.")]
    NO_SIGNATURE_FOUND,
    [Display(Name = "Ověřeni podpisu bylo úspěšné.")]
    PASSED,
    [Display(Name = "Ověření podpisu bylo neúspěšné.")]
    FAILED
}

public enum VerifySubIndications
{
    [Display(Name = "Nastala chyba při získávání vydavatelských certifikátů.")]
    CERTIFICATE_CHAIN_GENERAL_FAILURE,
    [Display(Name = "Vydavatelské certifikáty použité při ověřování neodpovídají omezením související s podepisujícím certifikátem.")]
    CHAIN_CONSTRAINTS_FAILURE,
    [Display(Name = "Kryptografické algoritmy použité při podepisování nesplňují dnešní minimální bezpečnostní požadavky (krátký klíč atp.) a podpis byl " +
                    "vytvořen poté co tyto algoritmy byly vyhlášeny za nedostatečné.")]
    CRYPTO_CONSTRAINTS_FAILURE,
    [Display(Name = "Kryptografické operace použité v podpisu nesplňují minimální bezpečnostní požadavky (délka podepisovacího klíče atp.).")]
    CRYPTO_CONSTRAINTS_FAILURE_NO_POE,
    [Display(Name = "Platnost podpisu vypršela, protože vypršela platnost certifikátu.")]
    EXPIRED,
    [Display(Name = "Formát podpisového bloku neodpovídá žádnému ze standardů.")]
    FORMAT_FAILURE,
    [Display(Name = "Hash otisk vložený v podpisu se neshoduje s otiskem vypočteným z podepisovaných dat.")]
    HASH_FAILURE,
    [Display(Name = "Nepodařilo se postavit celý řetezec certifikátů z podepisujícího certifikátu.")]
    NO_CERTIFICATE_CHAIN_FOUND,
    [Display(Name = "Chybí důkaz o existenci podpisu před tím než byla podepsaná data kompromitována.")]
    NO_POE,
    [Display(Name = "Nebyl nalezen žádný podepisující certifikát.")]
    NO_SIGNING_CERTIFICATE_FOUND,
    [Display(Name = "Podpis byl vytvořen certifikátem, který v čase podpisu ještě nebyl platný.")]
    NOT_YET_VALID,
    [Display(Name = "Podepisující certifikát ještě není platný nebo jeho platnost již vypršela a ověřující algoritmus nemůže zjistit, že čas podepsání leží " +
                    "v intervalu platnosti podpisového certifikátu.")]
    OUT_OF_BOUNDS_NO_POE,
    [Display(Name = "Podepisující certifikát ještě není platný nebo jeho platnost již vypršela a ověřující algoritmus nemůže zjistit, že čas podepsání leží " +
                    "v intervalu platnosti podpisového certifikátu.")]
    OUT_OF_BOUNDS_NOT_REVOKED,
    [Display(Name = "Požadovaná politika uvedená při ověřování nemohla být zpracována (nebyla dostupná, nešla zpracovat, rozdílné otiky, atd.).")]
    POLICY_PROCESSING_ERROR,
    [Display(Name = "Certifikát, kterým byla podepsána informace o zrušení podepisujícího certifikátu, ještě není platný nebo jeho platnost již vypršela v " +
                    "době ověřování podpisu a ověřující algoritmus nemůže zjistit, zda časová informace o zrušení certifikátu leží v době platnosti certifikátu, " +
                    "pomocí kterého byla tato informace podepsána.")]
    REVOCATION_OUT_OF_BOUNDS_NO_POE,
    [Display(Name = "Podepisující certifikát byl zrušen již před dobou než byl vytvořen digitální podpis.")]
    REVOKED,
    [Display(Name = "Některý z vydávajících certifikátů podepisujícího certifikátu byl zrušen.")]
    REVOKED_CA_NO_POE,
    [Display(Name = "Podepisujicí certifikát byl zrušen v době ověřování digitálního podpisu.")]
    REVOKED_NO_POE,
    [Display(Name = "Jeden nebo více atributů podpisu neodpovídá ověřovacím omezením.")]
    SIG_CONSTRAINTS_FAILURE,
    [Display(Name = "Digitální podpis se nepodařilo ověřit pomocí veřejného klíče podepisujícího certifikátu.")]
    SIG_CRYPTO_FAILURE,
    [Display(Name = "Dokument, který má obsahovat podrobnosti o požadované politiky není k dispozici.")]
    SIGNATURE_POLICY_NOT_AVAILABLE,
    [Display(Name = "Podepsané data nejsou k dispozici.")]
    SIGNED_DATA_NOT_FOUND,
    [Display(Name = "Byly porušeny omezení pořadí umístění podpisu a/nebo časových razítek.")]
    TIMESTAMP_ORDER_FAILURE,
    [Display(Name = "Všechny omezení nemohly být splněny pomocí dostupných informací.")]
    TRY_LATER
}

[JsonObject(MissingMemberHandling = MissingMemberHandling.Ignore)]
public class SignatureInfo
{
    [JsonProperty("signatureTime")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? SignatureTime { get; set; }

    [JsonProperty("signatureFormat")]
    public string SignatureFormat { get; set; }

    [JsonProperty("indication")]
    [JsonConverter(typeof(StringEnumConverter))]
    public VerifyIndications? Indication { get; set; }

    [JsonProperty("subIndication")]
    [JsonConverter(typeof(StringEnumConverter))]
    public VerifySubIndications? SubIndication { get; set; }

    [JsonProperty("errors")]
    public List<string> Errors { get; set; }

    [JsonProperty("warnings")]
    public List<string> Warnings { get; set; }

    [JsonProperty("signatureValidityMax")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? SignatureValidityMax { get; set; }

    [JsonProperty("signedBy")]
    public string SignedBy { get; set; }

    [JsonProperty("signatureQualificationName")]
    public string SignatureQualificationName { get; set; }

    [JsonProperty("signatureQualificationLabel")]
    public string SignatureQualificationLabel { get; set; }

    [JsonProperty("validationTime")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? ValidationTime { get; set; }
}

[JsonObject]
public class BlockchainVerifyRes
{
    [JsonProperty("proofId")]
    public string ProofId { get; set; }

    [JsonProperty("submitted")]
    public string SubmitTime { get; set; }

    [JsonProperty("hash")]
    public string ProofHash { get; set; }
    
    [JsonProperty("status")] 
    [JsonConverter(typeof(StringEnumConverter))]
    public BlockchainVerifyStatus? VerifyRes { get; set; }
}

public enum BlockchainVerifyStatus
{
    [Display(Name = "Záznam je validní. Ověřovaná data byla podepsána certifikátem, který byl předán při ověřování.")]
    Valid,
    [Display(Name = "Záznam čeká na uložení do systému blockchain. Proces uložení záznamu do systému blockchain může trvat delší dobu. Zkuste provést ověření později.")]
    Pending,
    [Display(Name = "Záznam není validní.")]
    Invalid,
    [Display(Name = "Záznam byl předložen systému blockchain k uložení. Proces uložení záznamu do systému blockchain může trvat delší dobu. Zkuste provést ověření později.")]
    Submitted,
    [Display(Name = "Záznam čeká na uložení do systému blockchain. Proces uložení záznamu do systému blockchain může trvat delší dobu. Zkuste provést ověření později.")]
    Confirmed,
    [Display(Name = "Ověření v systému blockchain selhalo.")]
    Failed,
    [Display(Name = "Záznam byl v systému blockchain očištěn.")]
    Purged
}
﻿using System.Text.Json.Serialization;

namespace CertiChain.Frontend.JsonObjects.Login
{
    public class LoginRegisterResp
    {
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("emailConfirmed")]
        public bool EmailConfirmed { get; set; }

        [JsonPropertyName("jwtToken")]
        public string JwtToken { get; set; }
    }
}

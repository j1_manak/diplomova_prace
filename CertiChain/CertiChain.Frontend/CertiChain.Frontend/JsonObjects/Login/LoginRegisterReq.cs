﻿
using System.Text.Json.Serialization;

namespace CertiChain.Frontend.JsonObjects.Login
{
    public class LoginRegisterReq
    {
        public LoginRegisterReq()
        {

        }

        public LoginRegisterReq(string email, string passwordHash)
        {
            Email = email;
            Password = passwordHash;
        }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}

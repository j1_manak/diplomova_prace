﻿using System.Text.Json.Serialization;

namespace CertiChain.Frontend.JsonObjects.User
{
    public class UserInfoResp
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("username")]
        public string Email { get; set; }

        [JsonPropertyName("emailConfirmed")]
        public bool EmailIsConfirmed { get; set; }
    }
}

﻿using System.Text.Json.Serialization;

namespace CertiChain.Frontend.JsonObjects.User
{
    public class ChangePwdReq
    {
        public ChangePwdReq()
        {

        }

        public ChangePwdReq(string oldPwd, string newPwd)
        {
            OldPwd = oldPwd;
            NewPwd = newPwd;
        }

        [JsonPropertyName("oldPassword")]
        public string OldPwd { get; set; }

        [JsonPropertyName("newPassword")]
        public string NewPwd { get; set; }
    }
}

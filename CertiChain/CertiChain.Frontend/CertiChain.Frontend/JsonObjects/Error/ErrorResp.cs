﻿using System.Text.Json.Serialization;

namespace CertiChain.Frontend.JsonObjects.Error
{
    public class ErrorResp
    {
        [JsonPropertyName("code")]
        public int Code { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("additionalInfo")]
        public string AdditionalInfo { get; set; }
    }
}

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CertiChain.Frontend.JsonObjects.Blockchain;

[JsonObject]
public class GetUserBlockchainRecordsResp
{
    [JsonProperty("itemsCount")]
    public int ItemCount { get; set; }

    [JsonProperty("items")]
    public List<UserBlockchainRecord> Items { get; set; }
}

[JsonObject]
public class UserBlockchainRecord
{
    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("creationDate")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? CreationTime { get; set; }

    [JsonProperty("hash")]
    public string RecordHash { get; set; }

    [JsonProperty("proofId")]
    public string ProofId { get; set; }
}
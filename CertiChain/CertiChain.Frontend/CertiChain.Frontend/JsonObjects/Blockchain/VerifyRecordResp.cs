using CertiChain.Frontend.JsonObjects.Signature;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CertiChain.Frontend.JsonObjects.Blockchain;

[JsonObject]
public class VerifyRecordResp
{
    [JsonProperty("verifyStatus")]
    [JsonConverter(typeof(StringEnumConverter))]
    public BlockchainVerifyStatus? VerifyStatus { get; set; }
}
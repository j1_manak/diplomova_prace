﻿

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CertiChain.Frontend.JsonObjects.Profiles
{
    public enum ProfileTypes
    {
        PAdES,
        CAdES,
        XAdES
    }

    public enum HashAlgorithms
    {
        SHA1,
        SHA256,
        SHA384,
        SHA512,
    }

    public class ProfileGeneralReqResp
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("profileType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileTypes ProfileType { get; set; }

        [JsonProperty("hashAlgorithm")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HashAlgorithms HashAlgorithm { get; set; }

        [JsonProperty("signerInfo")]
        public SignerInfoModelReqResp SignerInfo { get; set; }

        [JsonProperty("xadesProfile")]
        public ProfileXadesReqResp XadesProfile { get; set; }

        [JsonProperty("cadesProfile")]
        public ProfileCadesReqResp CadesProfile { get; set; }

        [JsonProperty("padesProfile")]
        public ProfilePadesReqResp PadesProfile { get; set; }

    }
}

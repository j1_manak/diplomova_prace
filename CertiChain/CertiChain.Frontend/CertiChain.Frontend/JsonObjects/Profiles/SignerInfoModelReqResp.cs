﻿using CertiChain.Frontend.Models.Profiles;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CertiChain.Frontend.JsonObjects.Profiles
{
    public class SignerInfoModelReqResp
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("country")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Country Country { get; set; }

        [JsonProperty("province")]
        public string Province { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("proofOfOrigin")]
        public bool IsProofOfOrigin { get; set; }

        [JsonProperty("proofOfCreation")]
        public bool IsProofOfCreation { get; set; }

    }
}

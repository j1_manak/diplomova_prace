﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CertiChain.Frontend.JsonObjects.Profiles
{
    public enum PadesTypes
    {
        B,
        T
    }

    public enum PadesVisibleSignatureTypes
    {
        [Display(Name = "Text")]
        Text,
        [Display(Name = "Obrázek")]
        Image,
        [Display(Name = "Text i obrázek")]
        TextAndImage
    }
    
    public enum BackgroundColors
    {
        [Display(Name = "Bílá")]
        White,
        [Display(Name = "Průhledná")]
        Transparent,
        [Display(Name = "Černá")]
        Black,
        [Display(Name = "Modrá")]
        Blue,
        [Display(Name = "Tyrkysová")]
        Cyan,
        [Display(Name = "Tmavě šedá")]
        DarkGray,
        [Display(Name = "Šedá")]
        Gray,
        [Display(Name = "Zelená")]
        Green,
        [Display(Name = "Světle šedá")]
        LightGray,
        [Display(Name = "Purpurová")]
        Magenta,
        [Display(Name = "Oranžová")]
        Orange,
        [Display(Name = "Růžová")]
        Pink,
        [Display(Name = "Červená")]
        Red,
        [Display(Name = "Žlutá")]
        Yellow
    }

    public class ProfilePadesReqResp
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("padesType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PadesTypes PadesType { get; set; }

        [JsonProperty("visible")]
        public bool IsVisible { get; set; }

        [JsonProperty("visibleSignatureType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PadesVisibleSignatureTypes? VisibleSignatureType { get; set; }

        [JsonProperty("signatureImage")]
        public byte[] SignatureImage { get; set; }

        [JsonProperty("pos_x")]
        public double? PosX { get; set; }

        [JsonProperty("pos_y")]
        public double? PosY { get; set; }

        [JsonProperty("width")]
        public double? Width { get; set; }

        [JsonProperty("height")]
        public double? Height { get; set; }

        [JsonProperty("page")]
        public int? Page { get; set; }

        [JsonProperty("signatureText")]
        public string SignatureText { get; set; }

        [JsonProperty("backgroundColor")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BackgroundColors? BackgroundColor { get; set; }

        [JsonProperty("signerName")]
        public string SignerName { get; set; }

        [JsonProperty("signLocation")]
        public string SignLocation { get; set; }

        [JsonProperty("signReason")]
        public string SignReason { get; set; }
    }
}

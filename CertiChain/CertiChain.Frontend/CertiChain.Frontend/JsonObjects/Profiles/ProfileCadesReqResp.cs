﻿using Newtonsoft.Json;

namespace CertiChain.Frontend.JsonObjects.Profiles
{
    public enum CadesTypes
    {
        B,
        T
    }

    public class ProfileCadesReqResp
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("detached")]
        public bool IsDetached { get; set; }

        [JsonProperty("cadesType")]
        public CadesTypes CadesType { get; set; }
    }
}

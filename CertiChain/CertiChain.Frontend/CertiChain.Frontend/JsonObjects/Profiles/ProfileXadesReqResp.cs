﻿using Newtonsoft.Json;

namespace CertiChain.Frontend.JsonObjects.Profiles
{
    public enum XadesTypes
    {
        B,
        T
    }

    public class ProfileXadesReqResp
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("xadesType")]
        public XadesTypes XadesType { get; set; }

        [JsonProperty("detached")]
        public bool IsDetached { get; set; }
    }
}

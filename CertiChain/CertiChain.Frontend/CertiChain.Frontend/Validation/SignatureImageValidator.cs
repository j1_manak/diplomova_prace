﻿using CertiChain.Frontend.JsonObjects.Profiles;
using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Validation
{
    public class SignatureImageValidator : ValidationAttribute
    {
        public SignatureImageValidator() : base()
        {

        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            try
            {
                var isVisibleProperty = validationContext.ObjectInstance.GetType().GetProperty("IsVisible");
                var isVisibleValue = isVisibleProperty.GetValue(validationContext.ObjectInstance, null);

                if (isVisibleValue != null && isVisibleValue is bool isVisible && isVisible == false)
                    return ValidationResult.Success;

                var VisibleSignatureTypeProperty = validationContext.ObjectInstance.GetType().GetProperty("PadesVisibleType");
                var visibleSignatureTypeValue = VisibleSignatureTypeProperty.GetValue(validationContext.ObjectInstance, null);
                if (visibleSignatureTypeValue != null && visibleSignatureTypeValue is PadesVisibleSignatureTypes visibleSignatureType)
                {
                    if ((visibleSignatureType == PadesVisibleSignatureTypes.Image || visibleSignatureType == PadesVisibleSignatureTypes.TextAndImage) &&
                        value == null)
                    {
                        return new ValidationResult("Obrázek podpisu je vyžadován");
                    }
                    else return ValidationResult.Success;
                }
                return ValidationResult.Success;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return ValidationResult.Success;
            }
        }
    }
}

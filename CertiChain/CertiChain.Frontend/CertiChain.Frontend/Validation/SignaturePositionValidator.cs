﻿using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Validation
{
    public class SignaturePositionValidator : ValidationAttribute
    {
        public SignaturePositionValidator() : base()
        {
        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            try
            {
               
                var isVisibleProperty = validationContext.ObjectInstance.GetType().GetProperty("IsVisible");
                var isVisibleValue = isVisibleProperty.GetValue(validationContext.ObjectInstance, null);

                if (isVisibleValue != null && isVisibleValue is bool isVisible && isVisible == false)
                    return ValidationResult.Success;

                if (value == null)
                    return new ValidationResult("Položka být vyplněna");

                var page = value.ToString();
                if (double.TryParse(page, out _) == false)
                    return new ValidationResult("Položka musí být zadána jako číslo");

                var pageNum = double.Parse(page);
                
                return pageNum == 0 ? new ValidationResult("Položka nemůže mít hodnotu 0") : ValidationResult.Success;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return ValidationResult.Success;
            }
        }
    }
}

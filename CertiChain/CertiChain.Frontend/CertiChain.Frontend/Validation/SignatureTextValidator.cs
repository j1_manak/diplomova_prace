﻿using CertiChain.Frontend.JsonObjects.Profiles;
using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Validation
{
    public class SignatureTextValidator : ValidationAttribute
    {
        public SignatureTextValidator() : base()
        {
        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            try
            {
                var isVisibleProperty = validationContext.ObjectInstance.GetType().GetProperty("IsVisible");
                var isVisibleValue = isVisibleProperty.GetValue(validationContext.ObjectInstance, null);

                if (isVisibleValue != null && isVisibleValue is bool isVisible && isVisible == false)
                    return ValidationResult.Success;

                var VisibleSignatureTypeProperty = validationContext.ObjectInstance.GetType().GetProperty("PadesVisibleType");
                var visibleSignatureTypeValue = VisibleSignatureTypeProperty.GetValue(validationContext.ObjectInstance, null);
                if (visibleSignatureTypeValue != null && visibleSignatureTypeValue is PadesVisibleSignatureTypes visibleSignatureType)
                {
                    if ((visibleSignatureType == PadesVisibleSignatureTypes.Text || visibleSignatureType == PadesVisibleSignatureTypes.TextAndImage) &&
                        (value == null) || string.IsNullOrWhiteSpace(value as string))
                    {
                        return new ValidationResult("Text podpisu je vyžadován");
                    }
                    else return ValidationResult.Success;
                }
                return ValidationResult.Success;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return ValidationResult.Success;
            }
           
        }
    }
}

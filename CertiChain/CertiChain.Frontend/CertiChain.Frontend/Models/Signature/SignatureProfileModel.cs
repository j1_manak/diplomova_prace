﻿using CertiChain.Frontend.JsonObjects.Profiles;

namespace CertiChain.Frontend.Models.Signature;

public class SignatureProfileModel
{
    public long Id { get; set; }
            
    public string Name { get; set; }

    public ProfileTypes ProfileType { get; set; }

    public bool IsChecked { get; set; }
}
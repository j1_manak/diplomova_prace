﻿using Blazorise;

namespace CertiChain.Frontend.Models.Signature;

public class VerifySignatureModel
{
    
    public IFileEntry SignedData { get; set; }

    public IFileEntry ExternaSignature { get; set; }
    
    public bool VerifyInBlockchain { get; set; }

    public IFileEntry SignerCertificate { get; set; }

    public string SignerCertPwd { get; set; }
    
}
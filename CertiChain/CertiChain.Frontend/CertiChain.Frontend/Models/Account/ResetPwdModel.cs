﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CertiChain.Frontend.Models.Account
{
    public class ResetPwdModel
    {
        public ResetPwdModel() { }

        [Required(ErrorMessage = "Emailová adresa musí být zadána.")]
        [EmailAddress(ErrorMessage = "Nejedná se o validní emailovou adresu.")]
        [JsonPropertyName("email")]
        public string Email { get; set; }
    }
}

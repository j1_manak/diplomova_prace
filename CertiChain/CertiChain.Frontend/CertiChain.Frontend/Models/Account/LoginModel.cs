﻿using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Models.Account
{
    public class LoginModel
    {
        public LoginModel()
        {

        }

        [Required(ErrorMessage = "Emailová adresa musí být vyplněna")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Heslo musí být vyplněno")]
        public string Password { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Models.Account
{
    public class RegisterModel
    {
        public RegisterModel()
        {
            Email = "";
            Password = "";
            PasswordAgain = "";
        }

        [Required(ErrorMessage = "Email musí být vyplněn")]
        [EmailAddress(ErrorMessage = "Nevalidní emailová adresa")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Heslo musí být vyplněno")]
        [StringLength(int.MaxValue, MinimumLength = 8, ErrorMessage = "Heslo musí mít alespoň 8 znaků")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Je potřeba zopakovat heslo pro potvrzení správnosti.")]
        [StringLength(int.MaxValue, MinimumLength = 8, ErrorMessage = "Heslo musí mít alespoň 8 znaků")]
        [DataType(DataType.Password)]
        [Compare(nameof(Password), ErrorMessage = "Hesla ne neshodují")]
        public string PasswordAgain { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Models.Account
{
    public class ChangePwdModel
    {
        public ChangePwdModel()
        {

        }

        [Required(ErrorMessage = "Staré heslo musí být vyplněno")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Nové heslo mus být vyplněno.")]
        [StringLength(100, ErrorMessage = "Nové heslo musí mít alespoň 8 znaků", MinimumLength = 8)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Je potřeba zopakovat heslo.")]
        [Compare(nameof(NewPassword), ErrorMessage = "Nové heslo a zopakované heslo se neshodují.")]
        public string NewPasswordAgain { get; set; }
    }
}

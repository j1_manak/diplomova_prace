﻿using CertiChain.Frontend.JsonObjects.Profiles;
using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Models.Profiles
{
    public class ProfileXadesModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Typ podpisu musí být určen")]
        public XadesTypes XadesType { get; set; }

        [Required(ErrorMessage = "Umístění podpisu musí být určeno")]
        public bool IsDetached { get; set; }
    }
}

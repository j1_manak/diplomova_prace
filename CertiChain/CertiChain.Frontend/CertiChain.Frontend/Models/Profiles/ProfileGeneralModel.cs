﻿using CertiChain.Frontend.JsonObjects.Profiles;
using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Models.Profiles
{
    public class ProfileGeneralModel
    {

        public long Id { get; set; }
        
        [Required(ErrorMessage = "Jméno prodilu musí být vyplněno.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Typ profilu musí být vyplněn")]
        public ProfileTypes ProfileType { get; set; }

        [Required(ErrorMessage = "Hash algoritmus musí být vyplněn")]
        public HashAlgorithms HashAlgorithm { get; set; }
    }
}

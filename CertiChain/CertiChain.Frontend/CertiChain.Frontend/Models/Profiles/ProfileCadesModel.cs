﻿using CertiChain.Frontend.JsonObjects.Profiles;
using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Models.Profiles
{
    public class ProfileCadesModel
    {
        
        public long Id { get; set; }

        [Required(ErrorMessage = "Musí být zvolen typ umístění podpisu")]
        public bool IsDetached { get; set; }

        [Required(ErrorMessage = "Musí být vybrán typ podpisu CAdES")]
        public CadesTypes CadesType { get; set; }
    }
}

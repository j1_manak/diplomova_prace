﻿using Blazorise;
using CertiChain.Frontend.JsonObjects.Profiles;
using CertiChain.Frontend.Validation;
using System.ComponentModel.DataAnnotations;

namespace CertiChain.Frontend.Models.Profiles
{
    public class ProfilePadesModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Typ podpisu PAdES musí být zvolen")]
        public PadesTypes PadesType { get; set; }

        [Required(ErrorMessage = "Musí být zvoleno, zda se jedná o viditelný podpis")]
        public bool IsVisible { get; set; }
        
        [Required(ErrorMessage = "Typ viditelného podpisu musí být zvolen")]
        public PadesVisibleSignatureTypes PadesVisibleType { get; set; }

        public byte[] SignatureImage { get; set; }

        [SignaturePositionValidator(ErrorMessage = "Umístění na ose x musí být vyplněno")]
        public double PosX { get; set; }

        [SignaturePositionValidator(ErrorMessage = "Umístění podpisu na ose y musí být vyplněno")]
        public double PosY { get; set; }

        [SignaturePositionValidator(ErrorMessage = "Šířka viditelného podpisu musí být vyplněna")]
        [Range(1,100, ErrorMessage = "Šířka viditelného pospisu musí být v rozmezí 1 až 100%")]
        public double Width { get; set; }

        [SignaturePositionValidator(ErrorMessage = "Výška viditelného podpisu musí být vyplněna")]
        [Range(1, 100, ErrorMessage = "Výška viditelného pospisu musí být v rozmezí 1 až 100%")]
        public double Height { get; set; }

        [SignaturePositionValidator(ErrorMessage = "Strana umístění podpisu musí být vyplněna")]
        public int Page { get; set; }

        [SignatureTextValidator(ErrorMessage = "Text podpisu musí být vyplněn")]
        public string SignatureText { get; set; }

        public BackgroundColors BackgroundColor { get; set; }

        public string SignerName { get; set; }

        public string SignLocation { get; set; }

        public string SignReason { get; set; }
    }
}

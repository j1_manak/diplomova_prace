﻿namespace CertiChain.Frontend.Models.Profiles
{
    public class SignerInfoModel
    {
        
        public long Id { get; set; }

        public Country Country { get; set; }

        public string Province { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string Role { get; set; }

        public bool IsProofOfOrigin { get; set; }

        public bool IsProofOfCreation { get; set; }
    }
}

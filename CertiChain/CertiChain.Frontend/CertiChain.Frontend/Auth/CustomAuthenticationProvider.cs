﻿using Blazored.LocalStorage;
using CertiChain.Frontend.Utils;
using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;

namespace CertiChain.Frontend.Auth
{
    public class CustomAuthenticationProvider : AuthenticationStateProvider
    {
        private readonly ILocalStorageService _localStorageService;
        private ClaimsPrincipal _claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity());

        public CustomAuthenticationProvider(ILocalStorageService localStorageService)
        {
            _localStorageService = localStorageService;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            string token = await _localStorageService.GetItemAsync<string>(JwtParser.JwtTokenKeyName);
            if (string.IsNullOrEmpty(token))
                return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity() { }));

            var claims = JwtParser.ParseClaimsFromJwt(token);
            if (claims is not null)
            {
                // kontrola expirace tokenu, pokud je uživatel se bude muset přihlásit znovu
                var expClaim = claims.FirstOrDefault(x => x.Type == "exp");
                Console.WriteLine(expClaim?.Value);
                if (expClaim is not null)
                {
                    var expTicks = long.Parse(expClaim.Value);
                    var expDate = new DateTime(1970, 1,1, 0, 0, 0, DateTimeKind.Local).AddMilliseconds(expTicks * 1000);
                    
                    if (DateTime.Now > expDate)
                    {
                        await _localStorageService.RemoveItemAsync(JwtParser.JwtTokenKeyName);
                        var anonymous = new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity() { }));
                        return anonymous;
                    }
                    else
                    {
                        var userClaimPrincipal = new ClaimsPrincipal(new ClaimsIdentity(JwtParser.ParseClaimsFromJwt(token), "Jwt auth from CertiChainAPI"));
                        var loginUser = new AuthenticationState(userClaimPrincipal);
                        return loginUser;
                    }
                }
            }
            
            return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity() { }));
        }

        public void Notify() => NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
    }
}

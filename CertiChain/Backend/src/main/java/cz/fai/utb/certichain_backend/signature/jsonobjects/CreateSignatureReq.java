package cz.fai.utb.certichain_backend.signature.jsonobjects;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

public class CreateSignatureReq implements Serializable {
    @Getter @Setter private Long profileId;
    @Getter @Setter private UUID userId;
    @Getter @Setter private byte[] dataToSign;
    @Getter @Setter private String fileName;
    @Getter @Setter private String fileMimeType;
    @Getter @Setter private String fileExtension;
    @Getter @Setter private byte[] signCertificate;
    @Getter @Setter private String certPwd;
    @Getter @Setter private String signCertMimeType;
    @Getter @Setter private boolean addToBlockchain;
}

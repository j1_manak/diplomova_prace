package cz.fai.utb.certichain_backend.profiles.controller;

import cz.fai.utb.certichain_backend.error.ErrorReturnCodes;
import cz.fai.utb.certichain_backend.error.dto.ErrorResp;
import cz.fai.utb.certichain_backend.profiles.jsonobjects.GeneralProfileModel;
import cz.fai.utb.certichain_backend.profiles.services.ProfileGeneralService;
import cz.fai.utb.certichain_backend.profiles.validators.ProfileValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@RestController
@RequestMapping("/profiles")
@CrossOrigin
public class ProfileController {

    private final ProfileGeneralService profileService;

    @Autowired
    public ProfileController(ProfileGeneralService profileService) {
        this.profileService = profileService;
    }

    @GetMapping(name = "/{userId}", path = "/{userId}")
    public ResponseEntity<?> getProfiles(@PathVariable UUID userId) {
        try {
            var profiles = profileService.getAllForUser(userId);
            return ResponseEntity.ok(profiles);
        } catch(EntityNotFoundException ex) {
            System.out.println(ex.toString());
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errNotFound, ex.getMessage()));
        } catch (Throwable ex) {
            System.out.println(ex.toString());
            return ResponseEntity.internalServerError()
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala interní chyba na serveru"));
        }
    }

    @GetMapping(name = "/{userId}/{profileId}", path = "/{userId}/{profileId}")
    public ResponseEntity<?> getFullProfile(@PathVariable UUID userId, @PathVariable long profileId) {
        try {
            var profileFull = profileService.getOneFull(profileId, userId);
            return ResponseEntity.ok(profileFull);
        } catch(EntityNotFoundException ex) {
            System.out.println(ex.toString());
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errNotFound, ex.getMessage()));
        } catch (Throwable ex) {
            System.out.println(ex.toString());
            return ResponseEntity.internalServerError()
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala interní chyba na serveru"));
        }
    }

    @PostMapping(name = "/{userId}/add", path = "/{userId}/add")
    public ResponseEntity<?> addProfile(@PathVariable UUID userId, @RequestBody GeneralProfileModel body) {
        try {
            var validationRes = ProfileValidator.ValidateGeneralProfileModel(body);
            if(validationRes != null) {
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, validationRes));
            }

            profileService.addNew(body, userId);
            return ResponseEntity.ok().build();
        } catch(EntityNotFoundException ex) {
            System.out.println(ex.toString());
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errNotFound, ex.getMessage()));
        } catch (Throwable ex) {
            System.out.println(ex.toString());
            return ResponseEntity.internalServerError()
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala interní chyba na serveru"));
        }
    }

    @PutMapping(name = "/{userId}/{profileId}", path = "/{userId}/{profileId}")
    public ResponseEntity<?> updateProfile(@PathVariable UUID userId, @PathVariable long profileId, @RequestBody GeneralProfileModel body) {
        try {
            var validationRes = ProfileValidator.ValidateGeneralProfileModel(body);
            if(validationRes != null) {
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, validationRes));
            }

            profileService.update(body, userId);
            return ResponseEntity.ok().build();
        } catch(EntityNotFoundException ex) {
            System.out.println(ex.toString());
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errNotFound, ex.getMessage()));
        } catch (Throwable ex) {
            System.out.println(ex.toString());
            return ResponseEntity.internalServerError()
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala interní chyba na serveru"));
        }
    }

    @DeleteMapping(name = "/{userId}/{profileId}", path = "/{userId}/{profileId}")
    public ResponseEntity<?> deleteProfile(@PathVariable UUID userId, @PathVariable long profileId) {
        try {
            profileService.delete(profileId, userId);
            return ResponseEntity.ok().build();
        } catch(EntityNotFoundException ex) {
            System.out.println(ex.toString());
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errNotFound, ex.getMessage()));
        } catch (Throwable ex) {
            System.out.println(ex.toString());
            return ResponseEntity.internalServerError()
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala interní chyba na serveru"));
        }
    }

}

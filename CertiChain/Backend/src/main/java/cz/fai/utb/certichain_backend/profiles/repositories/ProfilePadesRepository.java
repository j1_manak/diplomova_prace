package cz.fai.utb.certichain_backend.profiles.repositories;

import cz.fai.utb.certichain_backend.profiles.emtities.ProfilePadesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfilePadesRepository extends JpaRepository<ProfilePadesEntity, Long> {

}

package cz.fai.utb.certichain_backend.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class UserLoginResp implements Serializable {

    public UserLoginResp() {

    }

    public UserLoginResp(String email, boolean emailConfirmed, String jwtToken) {
        this.email = email;
        this.emailConfirmed = emailConfirmed;
        this.jwtToken = jwtToken;
    }

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private boolean emailConfirmed;

    @Getter
    @Setter
    private String jwtToken;
}

package cz.fai.utb.certichain_backend.signature.jsonobjects;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class CreateTransactionResp {
    @Getter @Setter private int ok;
    @Getter @Setter private String version;
    @Getter @Setter private MyDateTime dateTime;
    @Getter @Setter private String hash;
    @Getter @Setter private String proofId;
    @Getter @Setter private String status;
}


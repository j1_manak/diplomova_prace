package cz.fai.utb.certichain_backend.user.entitities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.context.annotation.Primary;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "users")
public class UserEntity {

    public UserEntity() {}

    public UserEntity(String email, String passwordHash) {
        this.email = email;
        this.passwordHash = passwordHash;
        publicId = UUID.randomUUID();
        isDeleted = false;
        emailConfirmed = false;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @Getter
    @Setter
    @Column(name = "private_id", updatable = false, nullable = false)
    private UUID publicId;

    @Getter
    @Setter
    @Column(name = "email", updatable = true, nullable = false)
    private String email;

    @Getter
    @Setter
    @Column(name = "password_hash", updatable = true, nullable = false)
    private String passwordHash;

    @Getter
    @Setter
    @Column(name = "is_deleted", updatable = true, nullable = false)
    private boolean isDeleted;

    @Getter
    @Setter
    @Column(name = "email_confirmed", updatable = true, nullable = false)
    private boolean emailConfirmed;
}

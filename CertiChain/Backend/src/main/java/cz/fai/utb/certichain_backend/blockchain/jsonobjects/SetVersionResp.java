package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class SetVersionResp implements Serializable {
    @Getter @Setter private int ok;
    @Getter @Setter private String response;
    @Getter @Setter private long version;
    @Getter @Setter private String status;
}

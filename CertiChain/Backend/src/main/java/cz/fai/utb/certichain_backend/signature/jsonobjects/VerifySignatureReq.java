package cz.fai.utb.certichain_backend.signature.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class VerifySignatureReq implements Serializable {

    @Getter @Setter private byte[] signedData;
    @Getter @Setter private String signedDataFileName;
    @Getter @Setter private String signedDataMimeType;
    @Getter @Setter private byte[] externalSignature;
    @Getter @Setter private String externalSignatureFileName;
    @Getter @Setter private String externalSignatureMimeType;
    @Getter @Setter private boolean verifyInBlockchain;
    @Getter @Setter private byte[] signerCertificate;
    @Getter @Setter private String signerCertMimeType;
    @Getter @Setter private String signerCertPwd;
}

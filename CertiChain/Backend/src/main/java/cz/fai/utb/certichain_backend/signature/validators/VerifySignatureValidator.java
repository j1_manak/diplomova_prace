package cz.fai.utb.certichain_backend.signature.validators;

import cz.fai.utb.certichain_backend.signature.jsonobjects.VerifySignatureReq;

public class VerifySignatureValidator {

    public static String validateVerifySignature(VerifySignatureReq req) {
        if (req == null) return "Žádost o ověření digitálního podpisu je prázdná";
        if (req.getSignedData() == null) return "Žádost neobsahuje podepsaná data k ověření";
        //if (req.getSignedDataMimeType() == null || req.getSignedDataMimeType().isEmpty())
          //  return "Žádost o ověření neobsahuje MIME podepisovaných dat";

        if (req.getExternalSignature() != null && (req.getExternalSignatureMimeType() == null || req.getExternalSignatureMimeType().isEmpty()))
            return "Žádost o ověření neobshauje MIME externího podpisu";

        if (req.isVerifyInBlockchain()) {
            if (req.getSignerCertificate() == null) return "Žádost o ověření neobsahuje certifikát podepisujícího";
            if (req.getSignerCertMimeType() == null || req.getSignerCertMimeType().isEmpty())
                return "Žádost o ověření neobsahuje MIME podepisovacího certifikátu";
        }

        return null;
    }
}

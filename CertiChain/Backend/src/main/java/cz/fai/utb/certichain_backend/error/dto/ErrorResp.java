package cz.fai.utb.certichain_backend.error.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ErrorResp implements Serializable {

    public ErrorResp(int code, String message, String additionalInfo) {
        this.code = code;
        this.message = message;
        this.additionalInfo = additionalInfo;
    }

    @Getter
    @Setter
    private int code;

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    private String additionalInfo;


}

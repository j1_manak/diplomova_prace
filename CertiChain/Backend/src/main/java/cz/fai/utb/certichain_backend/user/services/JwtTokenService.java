package cz.fai.utb.certichain_backend.user.services;

import cz.fai.utb.certichain_backend.user.entitities.UserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Function;

@Service
public class JwtTokenService implements Serializable {

    private long tokenValidity = 60 * 60 * 24 * 10 ;
    @Value("${jwt.secret}")
    private String secret;

    public JwtTokenService() {

    }

    //region PublicMethods

    public String getUsernameFromToken(String token) {
        var username = getClaimFromToken(token, Claims::getSubject);
        return username;
    }

    public Date getExpiratonDateFromToken(String token) {
        var date = getClaimFromToken(token, Claims::getExpiration);
        return date;
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        var claims = getALlClaims(token);
        return claimsResolver.apply(claims);
    }

    public boolean isTokenExpired(String token) {
        var expirationDate = getExpiratonDateFromToken(token);
        return new Date().before(expirationDate);
    }

    public String generateNewToken(UserEntity userInfo) {
        var claims = new HashMap<String, Object>();
        claims.put("username", userInfo.getEmail());
        claims.put("emailConfirmed", userInfo.isEmailConfirmed());
        claims.put("id", userInfo.getPublicId());

        var token = Jwts.builder()
                .setClaims(claims)
                .setSubject(userInfo.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + (tokenValidity * 1000)))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();

        return token;
    }

    public boolean validateToken(String token, UserDetails userInfo) {
        var usernameFromToken = getUsernameFromToken(token);
        var isValid = usernameFromToken.equals(userInfo.getUsername());
        return isValid && isTokenExpired(token);
    }

    //endregion

    //region PrivateMethods

    private Claims getALlClaims(String token) {
        var claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
        return claims.getBody();
    }

    //endregion
}

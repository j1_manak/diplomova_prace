package cz.fai.utb.certichain_backend.email.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Properties;
import java.util.UUID;

@Service
public class EmailService {

    private JavaMailSender emailSender;
    @Value("${spring.mail.username}")
    private String emailFrom;

    @Autowired
    public EmailService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }


    public void sendResetPwdEmail(String emailTo, String newPwd) throws Exception {
        var mailMessage = emailSender.createMimeMessage();
        var emailHelper = new MimeMessageHelper(mailMessage, "utf-8");

        emailHelper.setTo(emailTo);
        emailHelper.setFrom("CertiChain");
        emailHelper.setSubject("CertiChain - Resetování hesla pro uživatelský účet " + emailTo);
        emailHelper.setText(getResetPasswordMessage(emailTo, newPwd));

        emailSender.send(mailMessage);
    }

    public void sendActivationEmail(String emailTo, UUID id) throws Exception {
        var mailMessage = emailSender.createMimeMessage();
        var emailHelper = new MimeMessageHelper(mailMessage, "utf-8");

        emailHelper.setTo(emailTo);
        emailHelper.setFrom("CertiChain");
        emailHelper.setSubject("CertiChain - aktivační email");
        var baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();

        emailHelper.setText("Dobrý den,\n\nzde je k dispozici odkaz na ověření emailové adresy:\n" +
                baseUrl + "/user/confirmEmail/"+ id +
                "\n\nHezký den Vám přeje tým CertiChain");

        emailSender.send(mailMessage);
    }

    private String getResetPasswordMessage(String emailTo, String newPwd) {
        String message = "Dobrý den,\n\nnové heslo pro přihlášení do služby CertiChain pro uživatele " + emailTo + " je " + newPwd + ".\n\n" +
                "Po přihlášení do služby CertiChain doporučujeme přejít do nastavení uživatelského účtu a heslo si změnit z důvodu bezpečnosti Vašeho účtu.\n\n" +
                "Hezký den Vám přeje tým CertiChain";
        return message;
    }

}

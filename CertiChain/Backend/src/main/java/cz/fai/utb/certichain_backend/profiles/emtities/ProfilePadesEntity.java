package cz.fai.utb.certichain_backend.profiles.emtities;

import cz.fai.utb.certichain_backend.profiles.enums.PadesTypes;
import cz.fai.utb.certichain_backend.profiles.enums.PadesVisibleSignatureTypes;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ProfilePadesEntity {

    @Getter @Setter @Id @GeneratedValue
    private long id;
    @Getter @Setter private PadesTypes padesType;
    @Getter @Setter private boolean isVisible;
    @Getter @Setter private PadesVisibleSignatureTypes visibleSignatureType;
    @Getter @Setter @Column(length = Integer.MAX_VALUE)
    private byte[] signatureImage;
    @Getter @Setter private Double pos_x;
    @Getter @Setter private Double pos_y;
    @Getter @Setter private Double width;
    @Getter @Setter private Double height;
    @Getter @Setter private Integer page;
    @Getter @Setter private String signatureText;
    @Getter @Setter private String backgroundColor;
    @Getter @Setter private String signerName;
    @Getter @Setter private String signLocation;
    @Getter @Setter private String signReason;
}

package cz.fai.utb.certichain_backend.profiles.enums;

public enum PadesVisibleSignatureTypes {
    Text,
    Image,
    TextAndImage
}

package cz.fai.utb.certichain_backend.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

public class UserInfoResp implements Serializable {

    public UserInfoResp() {}

    public UserInfoResp(String username, boolean emailConfirmed, UUID id) {
        this.username = username;
        this.emailConfirmed = emailConfirmed;
        this.id = id;
    }

    @Getter
    @Setter
    private UUID id;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private boolean emailConfirmed;
}

package cz.fai.utb.certichain_backend.profiles.jsonobjects;

import cz.fai.utb.certichain_backend.profiles.enums.CadesTypes;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ProfileCades implements Serializable {

    @Getter @Setter private long id;
    @Getter @Setter private boolean isDetached;
    @Getter @Setter private CadesTypes cadesType;

}

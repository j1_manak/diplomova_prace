package cz.fai.utb.certichain_backend.signature.controller;

import cz.fai.utb.certichain_backend.error.ErrorReturnCodes;
import cz.fai.utb.certichain_backend.error.dto.ErrorResp;
import cz.fai.utb.certichain_backend.profiles.enums.ProfileTypes;
import cz.fai.utb.certichain_backend.profiles.services.ProfileGeneralService;
import cz.fai.utb.certichain_backend.signature.jsonobjects.CreateSignatureReq;
import cz.fai.utb.certichain_backend.signature.jsonobjects.CreateTransactionResp;
import cz.fai.utb.certichain_backend.signature.jsonobjects.VerifySignatureReq;
import cz.fai.utb.certichain_backend.blockchain.services.BlockChainService;
import cz.fai.utb.certichain_backend.signature.services.SignatureService;
import cz.fai.utb.certichain_backend.signature.validators.CreateSignatureValidator;
import cz.fai.utb.certichain_backend.signature.validators.VerifySignatureValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
@CrossOrigin
@RequestMapping("/signature")
public class SignatureController {

    private ProfileGeneralService profileService;
    private SignatureService signatureService;
    private BlockChainService blockChainService;

    @Autowired
    public SignatureController(ProfileGeneralService profileService, SignatureService signatureService, BlockChainService blockChainService) {

        this.profileService = profileService;
        this.signatureService = signatureService;
        this.blockChainService = blockChainService;
    }


    @PostMapping(name = "/create", path = "/create")
    public ResponseEntity<?> createSignature(@RequestBody CreateSignatureReq body) {
        try {

            var validationRes = CreateSignatureValidator.validateCreateSignatureReq(body);
            if(validationRes != null) {
                return ResponseEntity
                        .badRequest()
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, validationRes));
            }

            // throws exception if profile not found
            var profile = profileService.getOneFull(body.getProfileId(), body.getUserId());

            if(profile.getProfileType() == ProfileTypes.XAdES && !profile.getXadesProfile().isDetached() && body.getFileMimeType() != null &&
                    !body.getFileMimeType().equals("application/xml") && !body.getFileMimeType().equals("text/xml")) {
                return ResponseEntity
                        .badRequest()
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, "Pomocí profilu XAdES (popdis je součástí podepisovaných dat) lze " +
                                "podepsat pouze xml soubory"));
            }

            var signatureResp = signatureService.createSignature(profile, body);

            // TODO: add to blockchain if desired
            if(body.isAddToBlockchain()) {
                try {
                    CreateTransactionResp addToBCRes = null;
                    if (profile.getProfileType() == ProfileTypes.CAdES && profile.getCadesProfile().isDetached()) {
                        addToBCRes = blockChainService.addRecord(body.getDataToSign(), signatureResp.getSignature(), body.getSignCertificate(), body.getCertPwd());
                    } else if (profile.getProfileType() == ProfileTypes.XAdES && profile.getXadesProfile().isDetached()) {
                        addToBCRes = blockChainService.addRecord(body.getDataToSign(), signatureResp.getSignature(), body.getSignCertificate(), body.getCertPwd());
                    } else { // Pades, xades envelope, cades envelope,
                        addToBCRes = blockChainService.addRecord(signatureResp.getSignature(), null, body.getSignCertificate(), body.getCertPwd());
                    }

                    if (addToBCRes != null) {
                        blockChainService.addNewRecordToDb(addToBCRes, body.getUserId());
                        signatureResp.setAddedToBlockchain(true);
                    } else {
                        signatureResp.setAddedToBlockchain(false);
                        signatureResp.setAddToBlockchainError("Unknown error");
                    }
                } catch (Exception ex) {
                    signatureResp.setAddedToBlockchain(false);
                    signatureResp.setAddToBlockchainError(ex.getMessage());
                }
            }

            return ResponseEntity.ok(signatureResp);
        } catch (EntityNotFoundException exEntNotFound) {
            System.out.println(exEntNotFound);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errNotFound, "Podepisovací profil nebyl nalezen"));
        }
        catch (Throwable ex) {
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, ex.getMessage()));
        }
    }

    @PostMapping(name = "/verify", path = "/verify")
    public ResponseEntity<?> verifySignature(@RequestBody VerifySignatureReq body) {
        try {
            var validationRes = VerifySignatureValidator.validateVerifySignature(body);
            if (validationRes != null) {
                return ResponseEntity
                        .badRequest()
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, validationRes));
            }

            var response = signatureService.verifySignature(body);
            if(body.isVerifyInBlockchain()) {
                var bcVerifyRes = blockChainService.verifySignatureInBlockchain(body);
                response.setBlockchainVerifyRes(bcVerifyRes);
                response.setVerifiedInBlockchain(true);
            }
            return ResponseEntity.ok(response);
        } catch (Throwable ex) {
            System.out.println("Verify signature failed!!!");
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, ex.getMessage()));
        }
    }
}

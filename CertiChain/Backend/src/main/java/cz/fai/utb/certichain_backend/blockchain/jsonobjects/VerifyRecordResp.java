package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class VerifyRecordResp implements Serializable {
    @Getter @Setter private String verifyStatus;
}

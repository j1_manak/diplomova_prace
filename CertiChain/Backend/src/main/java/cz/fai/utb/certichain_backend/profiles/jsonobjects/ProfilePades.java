package cz.fai.utb.certichain_backend.profiles.jsonobjects;

import cz.fai.utb.certichain_backend.profiles.enums.PadesTypes;
import cz.fai.utb.certichain_backend.profiles.enums.PadesVisibleSignatureTypes;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ProfilePades implements Serializable {

    @Getter @Setter private long id;
    @Getter @Setter private PadesTypes padesType;
    @Getter @Setter private boolean isVisible;
    @Getter @Setter private PadesVisibleSignatureTypes visibleSignatureType;
    @Getter @Setter private byte[] signatureImage;
    @Getter @Setter private Double pos_x;
    @Getter @Setter private Double pos_y;
    @Getter @Setter private Double width;
    @Getter @Setter private Double height;
    @Getter @Setter private Integer page;
    @Getter @Setter private String signatureText;
    @Getter @Setter private String backgroundColor;
    @Getter @Setter private String signerName;
    @Getter @Setter private String signLocation;
    @Getter @Setter private String signReason;
}

package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ObjectId implements Serializable {
    @Getter @Setter @SerializedName("$oid") private String oid;
}

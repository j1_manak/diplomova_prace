package cz.fai.utb.certichain_backend.profiles.emtities;

import cz.fai.utb.certichain_backend.profiles.enums.XadesTypes;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ProfileXadesEntity {
    @Getter @Setter @Id @GeneratedValue
    private long id;
    @Getter @Setter private XadesTypes xadesType;
    @Getter @Setter private boolean isDetached;
}

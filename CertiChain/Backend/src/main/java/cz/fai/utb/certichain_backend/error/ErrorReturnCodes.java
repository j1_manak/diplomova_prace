package cz.fai.utb.certichain_backend.error;

public class ErrorReturnCodes {
    public static String errNotFound = "ErrNotFound";
    public static String errUnauthorized = "Unauthorized";
    public static String errInternalError = "InternalError";
    public static String errEmailAlreadyUsed = "EmailAlreadyUsed";
    public static String errBadRequest = "BadRequest";
    public static String errResendEmailFailed = "ResetEmailSendFail";
    public static String errUserNotFound = "UserNotFound";
    public static String errBadParameter = "InvalidParameter";
    public static String errForbidden = "Forbidden";
}

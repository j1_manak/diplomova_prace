package cz.fai.utb.certichain_backend.profiles.validators;

import cz.fai.utb.certichain_backend.profiles.enums.PadesVisibleSignatureTypes;
import cz.fai.utb.certichain_backend.profiles.enums.ProfileTypes;
import cz.fai.utb.certichain_backend.profiles.jsonobjects.GeneralProfileModel;

import java.util.Dictionary;

public class ProfileValidator {

    public static String ValidateGeneralProfileModel(GeneralProfileModel model) {
        if(model == null) return "Obsah požadavku je prázdný";
        if(model.getName().isEmpty()) return "Není zadáno jméno profilu";

        if(model.getProfileType() == ProfileTypes.CAdES && model.getCadesProfile() == null)
            return "Požadavek neobsahuje nastavení o požadovaném profilu (CAdES)";
        else if (model.getProfileType() == ProfileTypes.CAdES && model.getCadesProfile() != null) {
            // nothing to validate
            // if all ok for cades, return null
            return null;
        }

        if(model.getProfileType() == ProfileTypes.XAdES && model.getXadesProfile() == null)
            return "Požadavek neobsahuje nastavení o požadovaném profilu (XAdES)";
        else if (model.getProfileType() == ProfileTypes.XAdES && model.getXadesProfile() != null) {
            // nothing to validate
            // if all ok for xades, return null
            return null;
        }

        if(model.getProfileType() == ProfileTypes.PAdES && model.getPadesProfile() == null)
            return "Požadavek neobsahuje nastavení o požadovaném profilu (PAdES)";
        else if(model.getProfileType() == ProfileTypes.PAdES && model.getPadesProfile() != null) {
            var pp = model.getPadesProfile();
            if(!pp.isVisible()) return null; // nothing to validate when not visible

            if(pp.getVisibleSignatureType() == PadesVisibleSignatureTypes.Image || pp.getVisibleSignatureType() == PadesVisibleSignatureTypes.TextAndImage) {
                if(pp.getSignatureImage() == null) return "Není vybrán obrázek podpisu, přestože typ podpisu obrázek vyžaduje.";
                if(pp.getPos_x() == null || pp.getPos_y() == null) return "Není zadáno umístění podpisu";
                if(pp.getWidth() == null || pp.getHeight() == null) return "Není zadána velikost podpisu";
                if(pp.getPage() == null) return "Není zadána strana, na které se má podpis nacházet";
            }

            if(pp.getVisibleSignatureType() == PadesVisibleSignatureTypes.Text || pp.getVisibleSignatureType() == PadesVisibleSignatureTypes.TextAndImage) {
                if(pp.getBackgroundColor() == null || pp.getBackgroundColor().isEmpty()) return "Není vybrána barva pozadí pro podpis";
                if(pp.getSignatureText() == null || pp.getSignatureText().isEmpty()) return "Není nastaven text, který se má v podpisu zobrazit";
            }
            // if all ok for pades, return null

            return null;
        }

        return "Nepodporovaný typ podpisu";
    }

}

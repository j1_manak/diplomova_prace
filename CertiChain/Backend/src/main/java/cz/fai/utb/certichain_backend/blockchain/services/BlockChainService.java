package cz.fai.utb.certichain_backend.blockchain.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import cz.fai.utb.certichain_backend.blockchain.jsonobjects.*;
import cz.fai.utb.certichain_backend.blockchain.entities.BlockchainRecordEntity;
import cz.fai.utb.certichain_backend.signature.jsonobjects.CreateTransactionResp;
import cz.fai.utb.certichain_backend.signature.jsonobjects.VerifyInBlockchainRes;
import cz.fai.utb.certichain_backend.signature.jsonobjects.VerifySignatureReq;
import cz.fai.utb.certichain_backend.blockchain.repositories.BlockchainRecordRepository;
import cz.fai.utb.certichain_backend.signature.utils.CertUtils;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class BlockChainService {

    private MongoDatabase mongoDb;
    private BlockchainRecordRepository blockchainRecordRepository;
    private Gson gson;

    @Autowired
    public BlockChainService(BlockchainRecordRepository blockchainRecordRepository){
        this.blockchainRecordRepository = blockchainRecordRepository;
        var mongoClient = MongoClients.create("mongodb://pdbuser:click123@certichain.duckdns.org:27018/provendb");
        mongoDb = mongoClient.getDatabase("provendb");
        gson = new GsonBuilder().create();
    }

    public CreateTransactionResp addRecord(byte[] signedData, byte[] externalSignature, byte[] signingCert, String certPwd) throws Exception {
        var digest = MessageDigest.getInstance("SHA-256");

        var signedDataDigest = hashToHexString(digest.digest(signedData));
        var cert = CertUtils.getSigningCertificate(signingCert, certPwd);
        var certDigest =  hashToHexString(cert.getCertificate().getDigest(DigestAlgorithm.SHA256));

        String externalSignatureDigest = "";
        if (externalSignature != null) {
            externalSignatureDigest = hashToHexString(digest.digest(externalSignature));
        }

        // přidání záznamu do databáze
        var reqDoc = Document.parse("{" +
                "signedDataHash: \"" + signedDataDigest + "\"," +
                "externalDataHash: \"" + externalSignatureDigest + "\"," +
                "signCertHash: \"" + certDigest + "\"" +
                "}");

        var collection = mongoDb.getCollection("provendb");
        collection.insertOne(reqDoc);

        // po přidání záznamu vznikne nová verze, je potřeba získat její číslo pomocí příkazu setVersion: 'current'
        var version = getCurrentDbVersion();

        // a nakonec verzi odeslat do systému blockchain
        var submitProofReq = Document.parse("{submitProof: " + version + ", anchorType: 'ETH'}");
        var resp = mongoDb.runCommand(submitProofReq.toBsonDocument());
        var respJson = resp.toJson();

        CreateTransactionResp respObj = gson.fromJson(respJson, CreateTransactionResp.class);
        return respObj;
    }

    public BlockchainRecordEntity addNewRecordToDb(CreateTransactionResp obj, UUID userId) {
        var blockChainReportEntity = new BlockchainRecordEntity();
        blockChainReportEntity.setCreationDate(obj.getDateTime().getDate());
        blockChainReportEntity.setHash(obj.getHash());
        blockChainReportEntity.setProofId(UUID.fromString(obj.getProofId()));
        blockChainReportEntity.setUserId(UUID.fromString(userId.toString()));

        blockchainRecordRepository.save(blockChainReportEntity);
        return blockChainReportEntity;
    }

    public void deleteUserRecords(UUID userId) {
        var records = blockchainRecordRepository.findAll();
        if (records != null) {
            // Získání zánamů uživatele
            var ids = new ArrayList<Integer>();
            for (var record : records) {
                if (record.getUserId().toString().equals(userId.toString())) {
                    ids.add(record.getId());
                }
            }
            if (ids.size() > 0) {
                blockchainRecordRepository.deleteAllByIdInBatch(ids);
            }
        }
    }

    public Proof getProof(String recordId) {
        var reqObj = Document.parse("{getProof:'" + recordId + "'}");
        var respObj = mongoDb.runCommand(reqObj.toBsonDocument());

        var gsonDeserializer = new GsonBuilder().create();
        var deserializedObj = gsonDeserializer.fromJson(respObj.toJson(), ListProofsResp.class);

        if (deserializedObj.getProofs().size() > 0)
            return deserializedObj.getProofs().get(0);
        else
            return null;
    }

    public VerifyInBlockchainRes verifySignatureInBlockchain(VerifySignatureReq body) throws Exception {

        // vypočtení hashe z ověřovaných dat
        var digest = MessageDigest.getInstance("SHA-256");
        var signedDataDigest = hashToHexString(digest.digest(body.getSignedData()));
        String externalSignatureDigest = "";
        if (body.getExternalSignature() != null)
            externalSignatureDigest = hashToHexString(digest.digest(body.getExternalSignature()));

        String certDigest = null;
        if (body.getSignerCertMimeType().equals("application/x-pkcs12")) {
            var cert = CertUtils.getSigningCertificate(body.getSignerCertificate(), body.getSignerCertPwd());
            certDigest = hashToHexString(cert.getCertificate().getDigest(DigestAlgorithm.SHA256));
        } else if (body.getSignerCertMimeType().equals("application/x-pem-file") ||
            body.getSignerCertMimeType().equals("application/x-x509-ca-cert") || body.getSignerCertMimeType().equals("application/x-x509-user-cert")) {
            try {
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                var inputStream = new ByteArrayInputStream(body.getSignerCertificate());
                X509Certificate cert = (X509Certificate) cf.generateCertificate(inputStream);
                certDigest = hashToHexString(digest.digest(cert.getEncoded()));
            } catch (CertificateException e) {
                throw new Exception("Podepisovací certifikát není ve správném formátu.");
            }
        }
        else throw new Exception("Podepisovací certifikát není ve správném formátu.");

        // získání aktuální verze databáze
        var currVersion = getCurrentDbVersion();

        //vyhledání záznamu v databázi a zjištění jeho statusu v blockchain

        String reqJson = null;
        if(body.getExternalSignature() != null) {
            reqJson = "{" +
                "\"getDocumentProof\": { " +
                "\"collection\": \"provendb\", " +
                "\"filter\": { " +
                "\"signedDataHash\": \"" + signedDataDigest + "\"," +
                "\"externalDataHash\": \"" + externalSignatureDigest + "\"," +
                "\"signCertHash\": \"" + certDigest + "\" }," +
                "\"version\": " + currVersion + "," +
                "\"format\": \"json\"" +
                "}}";
        }
        else {
            reqJson = "{" +
                "\"getDocumentProof\": { " +
                "\"collection\": \"provendb\", " +
                "\"filter\": { " +
                "\"signedDataHash\": \"" + signedDataDigest + "\"," +
                "\"signCertHash\": \"" + certDigest + "\" }," +
                "\"version\": " + currVersion + "," +
                "\"format\": \"json\"" +
                "}}";
        }

        var verifyDocReq = Document.parse(reqJson);
        var verifyDocResp = mongoDb.runCommand(verifyDocReq.toBsonDocument());
        var verifyDocRespJson = verifyDocResp.toJson();
        var verifyDocObj = gson.fromJson(verifyDocRespJson, VerifyDocumentResp.class);

        if (verifyDocObj != null && verifyDocObj.getProofs() != null && verifyDocObj.getProofs().size() > 0) {
            var verifiedDoc = verifyDocObj.getProofs().get(0);
            var result = new VerifyInBlockchainRes();
            result.setHash(verifiedDoc.getDocumentHash());
            result.setStatus(verifiedDoc.getStatus());
            result.setProofId(verifiedDoc.getVersionProofId());
            result.setSubmitted(verifiedDoc.getProof().getHash_submitted_core_at());
            return result;
        }
        // shoda nenalezena, záznam není v systému blockchain
        return null;
    }

    public List<UserBlockchainRecord> getUserRecords(UUID userId) {
        var items = new ArrayList<UserBlockchainRecord>();
        var entities = blockchainRecordRepository.findAll();
        for(var entity : entities) {
            if (entity.getUserId().toString().equals(userId.toString())) {
                var item = new UserBlockchainRecord();
                item.setHash(entity.getHash());
                item.setId(entity.getId());
                item.setCreationDate(entity.getCreationDate());
                item.setProofId(entity.getProofId());
                items.add(item);
            }
        }
        return items;
    }

    private boolean searchForHash(List<ProofDocument> coll, String searchedHash) {
        for(var proof : coll) {
            if (proof.getName().equals(searchedHash)) return true;
        }
        return false;
    }

    private String hashToHexString(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private long getCurrentDbVersion() {
        var currentVersionReq = Document.parse("{setVersion: 'current'}");
        var currentVersionResp = mongoDb.runCommand(currentVersionReq);
        var setVersionRespObj = gson.fromJson(currentVersionResp.toJson(), SetVersionResp.class);

        return setVersionRespObj.getVersion();
    }

}

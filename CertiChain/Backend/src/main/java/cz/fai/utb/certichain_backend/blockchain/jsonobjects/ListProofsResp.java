package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class ListProofsResp {
    @Getter @Setter private int ok;
    @Getter @Setter private List<Proof> proofs;
}

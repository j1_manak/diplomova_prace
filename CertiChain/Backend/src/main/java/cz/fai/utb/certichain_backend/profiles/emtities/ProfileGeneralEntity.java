package cz.fai.utb.certichain_backend.profiles.emtities;

import cz.fai.utb.certichain_backend.profiles.enums.HashAlgorithm;
import cz.fai.utb.certichain_backend.profiles.enums.ProfileTypes;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Optional;
import java.util.UUID;

@Entity
public class ProfileGeneralEntity {
    @Id @GeneratedValue @Getter private long id;
    @Getter @Setter private String name;
    @Getter @Setter private ProfileTypes profileType;
    @Getter @Setter private UUID userId;
    @Getter @Setter @Column(nullable = true) private Long fullProfileId;
    @Getter @Setter @Column(nullable = true) private Long signerInfoId;
    @Getter @Setter private HashAlgorithm hashAlgorithm;
}

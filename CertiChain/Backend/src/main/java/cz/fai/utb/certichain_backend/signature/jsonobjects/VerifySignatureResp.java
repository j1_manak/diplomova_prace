package cz.fai.utb.certichain_backend.signature.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VerifySignatureResp implements Serializable {

    public VerifySignatureResp() {
        signatures = new ArrayList<>();
    }

    @Getter @Setter private List<SignatureInfo> signatures;
    @Getter @Setter private boolean verifiedInBlockchain;
    @Getter @Setter private VerifyInBlockchainRes blockchainVerifyRes;
}


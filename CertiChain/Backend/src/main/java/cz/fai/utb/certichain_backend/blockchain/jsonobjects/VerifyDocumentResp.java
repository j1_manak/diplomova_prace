package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

public class VerifyDocumentResp implements Serializable {
    @Getter @Setter private int ok;
    @Getter @Setter private List<VerifyDocumentItem> proofs;
}

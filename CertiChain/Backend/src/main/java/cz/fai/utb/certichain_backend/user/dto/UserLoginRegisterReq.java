package cz.fai.utb.certichain_backend.user.dto;

import lombok.Getter;
import lombok.Setter;

public class UserLoginRegisterReq {

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String password;
}

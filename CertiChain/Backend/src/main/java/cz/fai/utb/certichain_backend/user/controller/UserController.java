package cz.fai.utb.certichain_backend.user.controller;

import cz.fai.utb.certichain_backend.email.service.EmailService;
import cz.fai.utb.certichain_backend.error.ErrorReturnCodes;
import cz.fai.utb.certichain_backend.error.dto.ErrorResp;
import cz.fai.utb.certichain_backend.user.dto.*;
import cz.fai.utb.certichain_backend.user.entitities.UserEntity;
import cz.fai.utb.certichain_backend.user.services.JwtTokenService;
import cz.fai.utb.certichain_backend.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.Console;
import java.util.Random;
import java.util.UUID;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    private final UserService userService;
    private final JwtTokenService jwtTokenService;
    private final EmailService emailService;

    private AuthenticationManager authManager;
    private PasswordEncoder bcrypEncoder;


    @Autowired
    UserController(UserService userService, JwtTokenService jwtTokenService, AuthenticationManager authManager,
                   PasswordEncoder bcrypEncoder, EmailService emailService) {
        this.userService = userService;
        this.jwtTokenService = jwtTokenService;
        this.authManager = authManager;
        this.bcrypEncoder = bcrypEncoder;
        this.emailService = emailService;
    }

    @PostMapping(
            name = "/login",
            path = "/login",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> loginUser(@RequestBody UserLoginRegisterReq user) {
        try {
            var userEnt = userService.getUserByEmail(user.getEmail());
            if(userEnt != null && !userEnt.isDeleted()) {
                authManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
                var token = jwtTokenService.generateNewToken(userEnt);
                return ResponseEntity.ok(new UserLoginResp(userEnt.getEmail(), userEnt.isEmailConfirmed(), token));
            }
            else {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errNotFound, "Uživatel nebyl nalezen nebo bylo špatně zadáno heslo"));
            }
        } catch (AuthenticationException ex) {
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(new ErrorResp(HttpStatus.FORBIDDEN.value(), ErrorReturnCodes.errUnauthorized, ex.getMessage()));
        } catch(Throwable ex) {
            System.out.println(ex);
            return ResponseEntity.internalServerError().body(new ErrorResp(500, ErrorReturnCodes.errInternalError, ex.getMessage()));
        }
    }

    @PostMapping(
            name= "/register",
            path = "/register",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> registerUser(@RequestBody UserLoginRegisterReq user) {
        try {
            var userEnt = userService.getUserByEmail(user.getEmail());
            if (userEnt == null || userEnt.isDeleted()) {
                // user with received email not exist or been deleted in the past. We can do registration
                var pwdHash = bcrypEncoder.encode(user.getPassword());
                var newUserEnt = new UserEntity(user.getEmail(), pwdHash);
                var addedUser = userService.addUser(newUserEnt);
                authManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));

                var token = jwtTokenService.generateNewToken(addedUser);
                try {
                    emailService.sendActivationEmail(addedUser.getEmail(), addedUser.getPublicId());
                } catch (Throwable e) {
                    System.out.println(e);
                }

                // return user and session
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new UserLoginResp(addedUser.getEmail(), addedUser.isEmailConfirmed(), token));
            } else {
                // user with inserted email exists, return exception
                return new ResponseEntity<ErrorResp>(new ErrorResp(HttpStatus.CONFLICT.value(), ErrorReturnCodes.errEmailAlreadyUsed, "Uživatel se zadaným emailem již existuje")
                        , HttpStatus.CONFLICT);
            }
        } catch (Throwable ex) {
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala interní chyba na serveru"));
        }
    }

    @GetMapping(name = "/confirmEmail", path = "/confirmEmail/{id}" )
    public ModelAndView confirmEmail(@PathVariable UUID id) {
        try {
            // validate path variable
            if (id == null) {
                return new ModelAndView("redirect:https://certichain.duckdns.org/user/confirmEmail/failed");
            }

            var user = userService.getUserById(id);
            if(user == null) {
                return new ModelAndView("redirect:https://certichain.duckdns.org/user/confirmEmail/failed");
            }

            user.setEmailConfirmed(true);
            userService.updateUser(user);

            return new ModelAndView("redirect:https://certichain.duckdns.org/user/confirmEmail/success");

        } catch (Throwable ex) {
            System.out.println(ex);
            return new ModelAndView("redirect:https://certichain.duckdns.org/user/confirmEmail/failed");
        }
    }

    @PutMapping(
            name = "/forgottenPassword",
            path = "/forgottenPassword"
    )
    public ResponseEntity<?> resetPassword(@RequestBody ResetPwdReq resetPwdReq) {

        if (resetPwdReq == null || resetPwdReq.getEmail().isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, "Tělo žádosti neobsahuje požadovaná data"));
        }

        var user = userService.getUserByEmail(resetPwdReq.getEmail());
        if(user != null && !user.isDeleted()) {
            // generate new random pwd
            var newPwd = getRandomChars();
            var newPwdHash = bcrypEncoder.encode(newPwd);
            // send email
            try {
                emailService.sendResetPwdEmail(user.getEmail(), newPwd);
            } catch (Exception ex) {
                return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errResendEmailFailed, "Při odesílání emailu s novým heslem nastala chyba."));
            }
            // update password in database
            user.setPasswordHash(newPwdHash);
            try {
                userService.updateUser(user);
            } catch(Exception ex) {
                System.out.println(ex);
                throw ex;
            }

            // return update completed
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .build();
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errUserNotFound, "User with inserted email doesn't exist"));
        }
    }

    @PutMapping(name = "/changePassword/{publicId}", path = "/changePassword/{publicId}")
    public ResponseEntity<?> changePassword(@RequestBody ChangePwdReq body, @PathVariable UUID publicId) {
        // validate param
        try {
            if (publicId == null) {
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadParameter, "Nevalidní parametr předán v url adrese"));
            }

            // validate body
            var badRequest = false;
            if (body == null) badRequest = true;
            if (!badRequest && (body.getOldPassword().isEmpty() || body.getNewPassword().isEmpty())) badRequest = true;
            if (badRequest) {
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, "Žádost neobsahovala povinné položky (staré heslo a nové heslo)"));
            }

            // look for user in db
            var userEntity = userService.getUserById(publicId);
            if (userEntity == null) {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errUserNotFound, "Uživatel neexistuje"));
            }

            // compare old password
            var oldPwdIsMatch = bcrypEncoder.matches(body.getOldPassword(), userEntity.getPasswordHash());
            if (!oldPwdIsMatch) {
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body(new ErrorResp(HttpStatus.FORBIDDEN.value(), ErrorReturnCodes.errForbidden, "Staré heslo není správné"));
            }

            // set new password
            var newPwdHash = bcrypEncoder.encode(body.getNewPassword());
            userService.updatePassword(userEntity, newPwdHash);

            return ResponseEntity.ok().build();
        } catch (Throwable ex) {
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala interní chyba na serveru"));
        }
    }

    @PostMapping(name ="/delete", path = "/delete")
    public ResponseEntity<?> deleteUser(@RequestBody UserLoginRegisterReq body) {
        // request validation
        var badRequest = false;
        if(body == null) badRequest = true;
        if(!badRequest && (body.getEmail().isEmpty() || body.getPassword().isEmpty())) badRequest = true;

        if(badRequest) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, "Žádost neobsahovala všechny povinné argumenty (email a heslo)"));
        }

        var entity = userService.getUserByEmail(body.getEmail());
        if(entity == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errUserNotFound, "Uživatel nebyl nalezen"));
        }

        if(!bcrypEncoder.matches(body.getPassword(), entity.getPasswordHash())) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(new ErrorResp(HttpStatus.FORBIDDEN.value(), ErrorReturnCodes.errForbidden, "Nesprávné heslo"));
        }

        var res = userService.deleteUser(entity);
        if(res) {
            return ResponseEntity.ok().build();
        } else {

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala neznámá chyba při mazání uřivatele"));
        }
    }

    @GetMapping(name = "/resendActivationEmail", path = "/resendActivationEmail/{publicId}")
    public ResponseEntity<?> resendAcEmail(@PathVariable UUID publicId) {
        try {
            // validate path variable
            if (publicId == null) {
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadParameter, "Nevalidní parametr předán v url adrese"));
            }

            var entity = userService.getUserById(publicId);
            if (entity == null) {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errUserNotFound, "Uživatel nebyl nalezen"));
            }

            emailService.sendActivationEmail(entity.getEmail(), entity.getPublicId());

            return ResponseEntity.ok().build();

        } catch (Throwable ex) {
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala neznámá chyba při mazání uřivatele"));
        }
    }

    @GetMapping(name = "/info", path = "info/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserInfo(@PathVariable UUID id) {
        try {
            // validate path variable
            if (id == null) {
                return ResponseEntity
                        .status(HttpStatus.BAD_REQUEST)
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadParameter, "Nevalidní parametr předán v url adrese"));
            }

            var entity = userService.getUserById(id);
            if (entity == null) {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errUserNotFound, "Uživatel nebyl nalezen"));
            }

            var respObject = new UserInfoResp(entity.getEmail(), entity.isEmailConfirmed(), entity.getPublicId());

            return ResponseEntity.ok().body(respObject);
        } catch (Exception ex) {
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, "Nastala neznámá chyba při mazání uřivatele"));
        }
    }

    private String getRandomChars() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 8;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString;
    }
}

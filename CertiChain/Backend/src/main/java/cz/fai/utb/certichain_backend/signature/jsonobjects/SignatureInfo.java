package cz.fai.utb.certichain_backend.signature.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SignatureInfo implements Serializable {
    @Getter @Setter private Date signatureTime;
    @Getter @Setter private String signatureFormat;
    @Getter @Setter private String indication;
    @Getter @Setter private String subIndication;
    @Getter @Setter private List<String> errors;
    @Getter @Setter private List<String> warnings;
    @Getter @Setter private Date signatureValidityMax;
    @Getter @Setter private String signedBy;
    @Getter @Setter private String signatureQualificationName;
    @Getter @Setter private String signatureQualificationLabel;
    @Getter @Setter private Date validationTime;
}

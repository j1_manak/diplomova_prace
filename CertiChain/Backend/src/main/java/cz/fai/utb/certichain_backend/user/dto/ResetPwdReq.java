package cz.fai.utb.certichain_backend.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ResetPwdReq implements Serializable {

    @Getter
    @Setter
    private String email;
}

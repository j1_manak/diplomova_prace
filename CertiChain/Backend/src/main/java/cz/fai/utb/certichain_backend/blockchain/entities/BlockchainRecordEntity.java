package cz.fai.utb.certichain_backend.blockchain.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class BlockchainRecordEntity {
    @Getter @Setter @Id @GeneratedValue private Integer id;
    @Getter @Setter private UUID userId;
    @Getter @Setter private String creationDate;
    @Getter @Setter private String hash;
    @Getter @Setter private UUID proofId;
}

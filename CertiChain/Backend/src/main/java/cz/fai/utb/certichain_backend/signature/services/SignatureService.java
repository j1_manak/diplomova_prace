package cz.fai.utb.certichain_backend.signature.services;

import cz.fai.utb.certichain_backend.profiles.enums.*;
import cz.fai.utb.certichain_backend.profiles.jsonobjects.*;
import cz.fai.utb.certichain_backend.signature.jsonobjects.*;

import cz.fai.utb.certichain_backend.signature.utils.CertUtils;
import eu.europa.esig.dss.cades.CAdESSignatureParameters;
import eu.europa.esig.dss.cades.signature.CAdESService;
import eu.europa.esig.dss.enumerations.*;
import eu.europa.esig.dss.model.*;
import eu.europa.esig.dss.pades.PAdESSignatureParameters;
import eu.europa.esig.dss.pades.signature.PAdESService;
import eu.europa.esig.dss.pdf.pdfbox.PdfBoxDocumentReader;
import eu.europa.esig.dss.service.crl.OnlineCRLSource;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import eu.europa.esig.dss.service.http.commons.FileCacheDataLoader;
import eu.europa.esig.dss.service.ocsp.OnlineOCSPSource;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;
import eu.europa.esig.dss.spi.DSSUtils;
import eu.europa.esig.dss.spi.client.http.IgnoreDataLoader;
import eu.europa.esig.dss.spi.tsl.TrustedListsCertificateSource;
import eu.europa.esig.dss.spi.x509.CommonCertificateSource;
import eu.europa.esig.dss.spi.x509.CommonTrustedCertificateSource;
import eu.europa.esig.dss.spi.x509.tsp.TSPSource;
import eu.europa.esig.dss.token.KSPrivateKeyEntry;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import eu.europa.esig.dss.tsl.job.TLValidationJob;
import eu.europa.esig.dss.tsl.source.LOTLSource;
import eu.europa.esig.dss.tsl.sync.AcceptAllStrategy;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;

import eu.europa.esig.dss.validation.SignedDocumentValidator;
import eu.europa.esig.dss.xades.XAdESSignatureParameters;
import eu.europa.esig.dss.xades.signature.XAdESService;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.stereotype.Service;

import javax.transaction.NotSupportedException;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

@Service
public class SignatureService {

    private TrustedListsCertificateSource certTrustedList;

    public SignatureService() {
        createValidationJob();
    }

    public CreateSignatureResp createSignature(GeneralProfileModel signingProfile, CreateSignatureReq signatureReq) throws Exception {
        switch (signingProfile.getProfileType()){
            case PAdES:
                return createPadesSignature(signingProfile.getPadesProfile(), signingProfile.getHashAlgorithm(), 
                        signatureReq, signingProfile.getSignerInfo());
            case XAdES:
                return  createXadesSignature(signingProfile.getXadesProfile(), signingProfile.getHashAlgorithm(),
                        signatureReq, signingProfile.getSignerInfo());
            case CAdES:
                return createCadesSignature(signingProfile.getCadesProfile(), signingProfile.getHashAlgorithm(),
                        signatureReq, signingProfile.getSignerInfo());
            default:
                throw new NotSupportedException(signingProfile.getProfileType() + "není podporován");
        }
    }

    public VerifySignatureResp verifySignature(VerifySignatureReq req) {

        var commonVerifier = new CommonCertificateVerifier();
        commonVerifier.setCrlSource(new OnlineCRLSource());
        commonVerifier.setOcspSource(new OnlineOCSPSource());
        //commonVerifier.setAIASource(new DefaultAIASOurce());

        commonVerifier.addTrustedCertSources(certTrustedList);

        var verifiedDoc = new InMemoryDocument(req.getSignedData(), req.getSignedDataFileName(),
                MimeType.fromMimeTypeString(req.getSignedDataMimeType()));

        SignedDocumentValidator validator = null;
        if (req.getExternalSignature() == null) {
            validator = SignedDocumentValidator.fromDocument(verifiedDoc);
        } else {
            var extSig = new InMemoryDocument(req.getExternalSignature(), req.getExternalSignatureFileName(),
                    MimeType.fromMimeTypeString(req.getExternalSignatureMimeType()));
            validator = SignedDocumentValidator.fromDocument(extSig);
            var detachedData = new ArrayList<DSSDocument>();
            detachedData.add(verifiedDoc);

            validator.setDetachedContents(detachedData);
        }


        validator.setCertificateVerifier(commonVerifier);

        var reports = validator.validateDocument();
        var simpleReport = reports.getSimpleReport();

        var resp = new VerifySignatureResp();


        var detailReport = reports.getDetailedReport();
        var diagnosticData = reports.getDiagnosticData();

        var signatureIdsList = simpleReport.getSignatureIdList();
        for(var signId : signatureIdsList) {
            var sigInfo = new SignatureInfo();

            sigInfo.setSignatureTime(simpleReport.getSigningTime(signId));
            sigInfo.setIndication(detailReport.getBasicValidationIndication(signId).name());
            var errors = simpleReport.getErrors(signId);
            sigInfo.setErrors(errors);
            var warnings = simpleReport.getWarnings(signId);
            sigInfo.setWarnings(warnings);
            sigInfo.setSignatureValidityMax(simpleReport.getSignatureExtensionPeriodMax(signId));
            sigInfo.setSignatureFormat(simpleReport.getSignatureFormat(signId).name());
            sigInfo.setSignedBy(simpleReport.getSignedBy(signId));

            var qualification = simpleReport.getSignatureQualification(signId);
            if (qualification != null) {
                sigInfo.setSignatureQualificationLabel(qualification.getLabel());
                sigInfo.setSignatureQualificationName(qualification.name());
            }

            sigInfo.setValidationTime(simpleReport.getValidationTime());

            if(simpleReport.getSubIndication(signId) != null)
                sigInfo.setSubIndication(simpleReport.getSubIndication(signId).name());

            resp.getSignatures().add(sigInfo);
        }

        return resp;
    }

    private CreateSignatureResp createPadesSignature(ProfilePades padesSettings, HashAlgorithm hashAlgorithm, CreateSignatureReq signatureReq,
                                                     SignerInfoReqModel signerInfo) throws Exception {

        var padesParams = new PAdESSignatureParameters();

        // set signature level
        padesParams.setSignatureLevel(getPadesSignatureLevel(padesSettings.getPadesType()));

        // hash algorithm
        padesParams.setDigestAlgorithm(getHashAlgorithm(hashAlgorithm));

        // set sign cert and chain
        var dssCert = CertUtils.getSigningCertificate(signatureReq.getSignCertificate(), signatureReq.getCertPwd());
        padesParams.setSigningCertificate(dssCert.getCertificate());
        padesParams.setCertificateChain(dssCert.getCertificateChain());

        // set signer info if available
        if (signerInfo != null) {
            setBlevelParams(signerInfo, padesParams.bLevel());
        }

        // add reason if not null
        if(padesSettings.getSignReason() != null && !padesSettings.getSignReason().isEmpty())
            padesParams.setReason(padesSettings.getSignReason());
        // add location if not null
        if(padesSettings.getSignLocation() != null && !padesSettings.getSignLocation().isEmpty())
            padesParams.setLocation(padesSettings.getSignLocation());
        // add signer name if not null
        if(padesSettings.getSignerName() != null && !padesSettings.getSignerName().isEmpty())
            padesParams.setSignerName(padesSettings.getSignerName());

        // set visible settings if profile is visible
        if(padesSettings.isVisible()) {
            var visibleParams = padesParams.getImageParameters();

            var pdfReader = new PdfBoxDocumentReader(signatureReq.getDataToSign(), "");
            var pageCount =  pdfReader.getNumberOfPages();

            // set signing page
            int signingPage = 1;
            if(padesSettings.getPage() == -1)
                signingPage = pageCount;
            else if (padesSettings.getPage() > pageCount)
                signingPage = pageCount;
            else
                signingPage = padesSettings.getPage();
            visibleParams.getFieldParameters().setPage(signingPage);

            var pageWidth = pdfReader.getPageBox(signingPage).getWidth();
            var pageHeight = pdfReader.getPageBox(signingPage).getHeight();

            // compute width of signature
            var signatureWidth = (float)(padesSettings.getWidth() / 100) * pageWidth;
            visibleParams.getFieldParameters().setWidth(signatureWidth);

            // compute height of signature
            var signatureHeight = (float)(padesSettings.getHeight() / 100) * pageHeight;
            visibleParams.getFieldParameters().setHeight(signatureHeight);

            // compute pos x
            var posX = (float)(padesSettings.getPos_x() / 100) * pageWidth;
            visibleParams.getFieldParameters().setOriginX(posX);

            // compute pos y
            var posY = (float)(padesSettings.getPos_y() / 100) * pageHeight;
            visibleParams.getFieldParameters().setOriginY(posY);

            // set settings for text visible signature
            if(padesSettings.getVisibleSignatureType() == PadesVisibleSignatureTypes.Text ||
                    padesSettings.getVisibleSignatureType() == PadesVisibleSignatureTypes.TextAndImage) {
                var textParams = visibleParams.getTextParameters();
                // set text
                textParams.setText(padesSettings.getSignatureText());
                // set background color
                if (padesSettings.getBackgroundColor() != null && !padesSettings.getBackgroundColor().isEmpty()) {
                    var backColor = getBackgroundColor(padesSettings.getBackgroundColor());
                    if(backColor != null)
                        textParams.setBackgroundColor(backColor);
                }
            }

            // set settings for image visible signature
            if(padesSettings.getVisibleSignatureType() == PadesVisibleSignatureTypes.Image ||
                    padesSettings.getVisibleSignatureType() == PadesVisibleSignatureTypes.TextAndImage) {
                visibleParams.setImage(new InMemoryDocument(padesSettings.getSignatureImage()));
            }
        }

        var padesService = new PAdESService(new CommonCertificateVerifier());

        if (padesSettings.getPadesType() == PadesTypes.T) {
            padesService.setTspSource(getTspSource());
        }

        var documentToSign = new InMemoryDocument(signatureReq.getDataToSign(), signatureReq.getFileName(),
                MimeType.fromMimeTypeString(signatureReq.getFileMimeType()));

        var dataToSign = padesService.getDataToSign(documentToSign, padesParams);

        var signingToken = new Pkcs12SignatureToken(signatureReq.getSignCertificate(), 
                new KeyStore.PasswordProtection(signatureReq.getCertPwd().toCharArray()));
        var signatureValue = signingToken.sign(dataToSign, padesParams.getDigestAlgorithm(), dssCert);

        var signedDocument = padesService.signDocument(documentToSign, padesParams, signatureValue);
        var signature = DSSUtils.toByteArray(signedDocument);
        var signedDataMimeType = signedDocument.getMimeType();

        var signatureResp = new CreateSignatureResp();
        signatureResp.setSignature(signature);
        signatureResp.setSignatureMimeType(signedDataMimeType.getMimeTypeString());
        signatureResp.setSignatureFileExtensions("_signed.pdf");

        return  signatureResp;
    }

    private CreateSignatureResp createCadesSignature(ProfileCades cadesSettings, HashAlgorithm hashAlgorithm, CreateSignatureReq signatureReq,
                                                     SignerInfoReqModel signerInfo)
        throws Exception{

        // cades params
        CAdESSignatureParameters cadesParams = new CAdESSignatureParameters();
        // signature level
        cadesParams.setSignatureLevel(getCadesSignatureLevel(cadesSettings.getCadesType()));


        // signature packaging
        if(cadesSettings.isDetached())
            cadesParams.setSignaturePackaging(SignaturePackaging.DETACHED);
        else
            cadesParams.setSignaturePackaging(SignaturePackaging.ENVELOPING);

        // hash algorithm
        cadesParams.setDigestAlgorithm(getHashAlgorithm(hashAlgorithm));

        var certificate = CertUtils.getSigningCertificate(signatureReq.getSignCertificate(), signatureReq.getCertPwd());
        // set signing certificate
        cadesParams.setSigningCertificate(certificate.getCertificate());
        // set cert chain
        cadesParams.setCertificateChain(certificate.getCertificateChain());

        // add signer info if available
        if(signerInfo != null) {
            setBlevelParams(signerInfo, cadesParams.bLevel());
        }

        // create signature
        CommonCertificateVerifier verifier = new CommonCertificateVerifier();
        CAdESService service = new CAdESService(verifier);

        // set tsp source if level is T
        if(cadesSettings.getCadesType() == CadesTypes.T || cadesSettings.getCadesType() == CadesTypes.LTA) {
            // set tsp source
            service.setTspSource(getTspSource());
        }

        InMemoryDocument documentToSign = new InMemoryDocument(signatureReq.getDataToSign(), 
                signatureReq.getFileName(), MimeType.fromMimeTypeString(signatureReq.getFileMimeType()));

        var dataToSign = service.getDataToSign(documentToSign, cadesParams);

        var signingToken = new Pkcs12SignatureToken(signatureReq.getSignCertificate(), 
                new KeyStore.PasswordProtection(signatureReq.getCertPwd().toCharArray()));
        var signatureValue = signingToken.sign(dataToSign, cadesParams.getDigestAlgorithm(), certificate);

        var signedDocument = service.signDocument(documentToSign, cadesParams, signatureValue);
        var signature = DSSUtils.toByteArray(signedDocument);
        var signedDataMimeType = signedDocument.getMimeType();

        var signatureResp = new CreateSignatureResp();
        signatureResp.setSignature(signature);
        signatureResp.setSignatureMimeType(signedDataMimeType.getMimeTypeString());

        if(cadesSettings.isDetached())
            signatureResp.setSignatureFileExtensions(".p7s");
        else
            signatureResp.setSignatureFileExtensions("_signed" + signatureReq.getFileExtension());
        return signatureResp;
    }

    private CreateSignatureResp createXadesSignature(ProfileXades xadesSettings, HashAlgorithm hashAlgorithm, CreateSignatureReq signatureReq,
                                                     SignerInfoReqModel signerInfo) throws Exception {

        var xadesParams = new XAdESSignatureParameters();
        var dssCert = CertUtils.getSigningCertificate(signatureReq.getSignCertificate(), signatureReq.getCertPwd());

        // set sign cert and chain
        xadesParams.setSigningCertificate(dssCert.getCertificate());
        xadesParams.setCertificateChain(dssCert.getCertificateChain());

        // set hash alg
        xadesParams.setDigestAlgorithm(getHashAlgorithm(hashAlgorithm));

        // set signature packaging
        if(xadesSettings.isDetached())
            xadesParams.setSignaturePackaging(SignaturePackaging.DETACHED);
        else
            xadesParams.setSignaturePackaging(SignaturePackaging.ENVELOPED);

        // set signature level
        xadesParams.setSignatureLevel(getXadesSignatureLevel(xadesSettings.getXadesType()));

        // add signer info if available
        if(signerInfo != null) {
            setBlevelParams(signerInfo, xadesParams.bLevel());
        }

        var service = new XAdESService(new CommonCertificateVerifier());

        if(xadesSettings.getXadesType() == XadesTypes.T) {
            // add time stamp source
            service.setTspSource(getTspSource());
        }

        InMemoryDocument documentToSign = new InMemoryDocument(signatureReq.getDataToSign(), signatureReq.getFileName(),
                MimeType.fromMimeTypeString(signatureReq.getFileMimeType()));

        var dataToSign = service.getDataToSign(documentToSign, xadesParams);

        var signingToken = new Pkcs12SignatureToken(signatureReq.getSignCertificate(), 
                new KeyStore.PasswordProtection(signatureReq.getCertPwd().toCharArray()));
        var signatureValue = signingToken.sign(dataToSign, xadesParams.getDigestAlgorithm(), dssCert);

        var signedDocument = service.signDocument(documentToSign, xadesParams, signatureValue);
        var signature = DSSUtils.toByteArray(signedDocument);
        var signedDataMimeType = signedDocument.getMimeType();

        var signatureResp = new CreateSignatureResp();
        signatureResp.setSignature(signature);
        signatureResp.setSignatureMimeType(signedDataMimeType.getMimeTypeString());
        if(xadesSettings.isDetached()) signatureResp.setSignatureFileExtensions("_signature.xml");
        else signatureResp.setSignatureFileExtensions("_signed" + signatureReq.getFileExtension());

        return signatureResp;
    }


    private SignatureLevel getCadesSignatureLevel(CadesTypes cadesType){
        if (cadesType == CadesTypes.T) {
            return SignatureLevel.CAdES_BASELINE_T;
        }
        return SignatureLevel.CAdES_BASELINE_B; // B
    }

    private SignatureLevel getXadesSignatureLevel(XadesTypes xadesType) {
        if (xadesType == XadesTypes.T) {
            return SignatureLevel.XAdES_BASELINE_T;
        }
        return SignatureLevel.XAdES_BASELINE_B;
    }

    private SignatureLevel getPadesSignatureLevel(PadesTypes padesType) {
        if (padesType == PadesTypes.T) {
            return SignatureLevel.PAdES_BASELINE_T;
        }
        return SignatureLevel.PAdES_BASELINE_B;
    }

    private DigestAlgorithm getHashAlgorithm(HashAlgorithm hash) {
        switch(hash) {
            default: return DigestAlgorithm.SHA1;
            case SHA256: return DigestAlgorithm.SHA256;
            case SHA384: return DigestAlgorithm.SHA384;
            case SHA512: return DigestAlgorithm.SHA512;
        }
    }

    private TSPSource getTspSource(){
        var tspSource = new OnlineTSPSource("http://dss.nowina.lu/pki-factory/tsa/good-tsa"); // "http://timestamp.apple.com/ts01"
        return  tspSource;
    }

    private void setBlevelParams(SignerInfoReqModel signerInfo, BLevelParameters bParams) {
        var signerLoc = new SignerLocation();

        if(signerInfo.getCountry() != null && !signerInfo.getCountry().isEmpty())
            signerLoc.setCountry(signerInfo.getCountry());

        if(signerInfo.getProvince() != null && !signerInfo.getProvince().isEmpty())
            signerLoc.setStateOrProvince(signerInfo.getProvince());

        if(signerInfo.getCity() != null && !signerInfo.getCity().isEmpty())
            signerLoc.setLocality(signerInfo.getCity());

        if(signerInfo.getPostalCode() != null && !signerInfo.getPostalCode().isEmpty())
            signerLoc.setPostalCode(signerInfo.getPostalCode());

        bParams.setSignerLocation(signerLoc);

        if(signerInfo.getRole() != null && !signerInfo.getRole().isEmpty()) {
            List<String> roles = new ArrayList<>();
            if(signerInfo.getRole().contains(", ")) {
                var items = signerInfo.getRole().split(", ");
                for(var item : items) roles.add(item);
            } else if (signerInfo.getRole().contains(",")) {
                var items = signerInfo.getRole().split(",");
                for(var item : items) roles.add(item);
            } else if (signerInfo.getRole().contains("; ")) {
                var items = signerInfo.getRole().split("; ");
                for (var item : items) roles.add(item);
            } else if (signerInfo.getRole().contains(";")) {
                var items = signerInfo.getRole().split(";");
                for (var item : items) roles.add(item);
            } else {
                roles.add(signerInfo.getRole());
            }

            bParams.setClaimedSignerRoles(roles);
        }

        if(signerInfo.isProofOfCreation() || signerInfo.isProofOfOrigin()) {
            var commitments = new ArrayList<CommitmentType>();
            if(signerInfo.isProofOfOrigin())
                commitments.add(CommitmentTypeEnum.ProofOfOrigin);
            if(signerInfo.isProofOfCreation())
                commitments.add(CommitmentTypeEnum.ProofOfCreation);

            bParams.setCommitmentTypeIndications(commitments);
        }

    }

    private Color getBackgroundColor(String color) {
        switch (color) {
            default: return null; // transparent
            case "Black": return Color.black;
            case "Blue": return Color.blue;
            case "Cyan": return Color.cyan;
            case "DarkGray": return Color.darkGray;
            case "Gray": return Color.gray;
            case "Green": return Color.green;
            case "LightGray": return Color.lightGray;
            case "Magenta": return Color.magenta;
            case "Orange": return Color.orange;
            case "Pink": return Color.pink;
            case "Red": return Color.red;
            case "White": return Color.white;
            case "Yellow": return Color.yellow;
        }
    }

    private void createValidationJob() {
        try {
            certTrustedList = new TrustedListsCertificateSource();
            TLValidationJob job = new TLValidationJob();

            job.setTrustedListCertificateSource(certTrustedList);
            job.setSynchronizationStrategy(new AcceptAllStrategy());

            // set offline data loader
            FileCacheDataLoader offlineFileLoader = new FileCacheDataLoader();
            offlineFileLoader.setCacheExpirationTime(Long.MAX_VALUE);
            offlineFileLoader.setDataLoader(new IgnoreDataLoader());
            offlineFileLoader.setFileCacheDirectory(tlCacheDirectory());
            job.setOfflineDataLoader(offlineFileLoader);

            // set online data loader
            FileCacheDataLoader onlineFileLoader = new FileCacheDataLoader();
            onlineFileLoader.setCacheExpirationTime(0);
            onlineFileLoader.setDataLoader(new CommonsDataLoader());
            onlineFileLoader.setFileCacheDirectory(tlCacheDirectory());
            job.setOnlineDataLoader(onlineFileLoader);

            // add eu trusted lists
            var euLotlSource = new LOTLSource();
            euLotlSource.setUrl("https://ec.europa.eu/tools/lotl/eu-lotl.xml");
            euLotlSource.setCertificateSource(new CommonCertificateSource());
            euLotlSource.setPivotSupport(true);

            job.setListOfTrustedListSources(euLotlSource);

            job.onlineRefresh();

        } catch (Throwable ex) {
            System.out.println("Create validation job to download trusted lists failed!!!");
            System.out.println(ex);
        }
    }

    private File tlCacheDirectory() {
        File rootFolder = new File(System.getProperty("java.io.tmpdir"));
        File tslCache = new File(rootFolder, "dss-tsl-loader");
        if (tslCache.mkdirs()) {
            System.out.println("TL Cache folder : " + tslCache.getAbsolutePath());
        }
        return tslCache;
    }
}

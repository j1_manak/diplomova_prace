package cz.fai.utb.certichain_backend.signature.validators;

import cz.fai.utb.certichain_backend.signature.jsonobjects.CreateSignatureReq;

import java.io.ByteArrayInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class CreateSignatureValidator {

    public static String validateCreateSignatureReq(CreateSignatureReq req)  {

        if(req == null) return "Žádost neobsahuje žádná data";
        if(req.getProfileId() == null) return "Žádost neobsahuje id profilu";
        if(req.getUserId() == null) return "Žádost neobsahuje id uživatele";
        if(req.getDataToSign() == null) return "Žádost neobsahuje žádná data k podepsání";
        if(req.getSignCertificate() == null) return "Žádost neobsahuje podepisovací certifikát";
        if(req.getCertPwd().isEmpty()) return "Žádost neobsahuje heslo k podepisovacímu certifikátu";

        try {
            var keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(new ByteArrayInputStream(req.getSignCertificate()), req.getCertPwd().toCharArray());
        } catch (Exception ex) {
            return "Heslo k podepisovacímu certifikátu je špatné";
        }
        return null;
    }

}

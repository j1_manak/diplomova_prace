package cz.fai.utb.certichain_backend.blockchain.repositories;

import cz.fai.utb.certichain_backend.blockchain.entities.BlockchainRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlockchainRecordRepository extends JpaRepository<BlockchainRecordEntity, Integer> {

}

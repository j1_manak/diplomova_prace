package cz.fai.utb.certichain_backend.profiles.services;

import cz.fai.utb.certichain_backend.profiles.emtities.*;
import cz.fai.utb.certichain_backend.profiles.jsonobjects.*;
import cz.fai.utb.certichain_backend.profiles.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class ProfileGeneralService {

    private final ProfileGeneralRepository profileGeneralRepository;
    private final SignerInfoRepository signerInfoRepository;
    private final ProfileCadesRepository cadesRepository;
    private final ProfileXadesRepository xadesRepository;
    private final ProfilePadesRepository padesRepository;

    @Autowired
    public ProfileGeneralService(ProfileGeneralRepository profileGeneralRepository, SignerInfoRepository signerInfoRepository,
                                 ProfileCadesRepository cadesRepository, ProfileXadesRepository xadesRepository, ProfilePadesRepository padesRepository) {
        this.profileGeneralRepository = profileGeneralRepository;
        this.signerInfoRepository = signerInfoRepository;
        this.cadesRepository = cadesRepository;
        this.xadesRepository = xadesRepository;
        this.padesRepository = padesRepository;
    }

    //region PublicMethods

    public List<GeneralProfileModel> getAll() {
        var profiles = profileGeneralRepository.findAll();
        var models = new ArrayList<GeneralProfileModel>();
        for(var profileEnt : profiles) {
            var model = new GeneralProfileModel();
            model.setId(profileEnt.getId());
            model.setName(profileEnt.getName());
            model.setHashAlgorithm(profileEnt.getHashAlgorithm());
            model.setProfileType(profileEnt.getProfileType());

            models.add(model);
        }
        return models;
    }

    public List<GeneralProfileModel> getAllForUser(UUID userId) {
        var profiles = profileGeneralRepository.findAll();
        var models = new ArrayList<GeneralProfileModel>();
        for(var profileEnt : profiles) {
            if(profileEnt.getUserId().equals(userId)) {
                var model = new GeneralProfileModel();
                model.setId(profileEnt.getId());
                model.setName(profileEnt.getName());
                model.setHashAlgorithm(profileEnt.getHashAlgorithm());
                model.setProfileType(profileEnt.getProfileType());

                models.add(model);
            }
        }
        return models;
    }

    public GeneralProfileModel getOneFull(long id, UUID userId) throws EntityNotFoundException, Exception {
        var generaProfileEnt = profileGeneralRepository.findById(id);
        if(generaProfileEnt.isPresent()) {
            var genProfile = generaProfileEnt.get();

            if(!genProfile.getUserId().equals(userId))
                throw new EntityNotFoundException("GeneralProfileEntity with id = " + id + " not found");

            var model = new GeneralProfileModel();
            model.setId(genProfile.getId());
            model.setProfileType(genProfile.getProfileType());
            model.setName(genProfile.getName());
            model.setHashAlgorithm(genProfile.getHashAlgorithm());
            // append signer info if available
            if (genProfile.getSignerInfoId() != null) {
                var signerInfoEnt = signerInfoRepository.findById(genProfile.getSignerInfoId());
                if(signerInfoEnt.isPresent()) {
                    var signerInfoData = signerInfoEnt.get();
                    var si = new SignerInfoReqModel();
                    si.setId(signerInfoData.getId());
                    si.setCity(signerInfoData.getCity());
                    si.setCountry(signerInfoData.getCountry());
                    si.setPostalCode(signerInfoData.getPostalCode());
                    si.setProvince(signerInfoData.getProvince());
                    si.setProofOfCreation(signerInfoData.isProofOfCreation());
                    si.setProofOfOrigin(signerInfoData.isProofOfOrigin());
                    si.setRole(signerInfoData.getRole());

                    model.setSignerInfo(si);
                }
            }
            // get sign detail setting using type
            switch(genProfile.getProfileType()) {
                case CAdES:
                    var cades = getCadesSettings(genProfile.getFullProfileId());
                    if(cades == null) throw new EntityNotFoundException("Cades settings not found");
                    model.setCadesProfile(cades);
                    break;
                case PAdES:
                    var pades = getPadesSettings(genProfile.getFullProfileId());
                    if(pades == null) throw new EntityNotFoundException("Pades settings not found");
                    model.setPadesProfile(pades);
                    break;
                case XAdES:
                    var xades = getXadesSettings(genProfile.getFullProfileId());
                    if(xades == null) throw new EntityNotFoundException("Xades settings not found");
                    model.setXadesProfile(xades);
                    break;
                default:
                    throw new Exception("UnsupportedType - " + genProfile.getProfileType().toString());
            }

            return model;
        } else throw new EntityNotFoundException("GeneralProfileEntity with id = " + id + " not found");
    }

    public ProfileGeneralEntity addNew(GeneralProfileModel newProfile, UUID userId) throws Exception {
        var gpe = new ProfileGeneralEntity();

        // set general info
        gpe.setName(newProfile.getName());
        gpe.setProfileType(newProfile.getProfileType());
        gpe.setUserId(userId);
        gpe.setHashAlgorithm(newProfile.getHashAlgorithm());

        // set signer info (not required)
        if(newProfile.getSignerInfo() != null) {
            createSignerInfo(newProfile, gpe);
        }

        // save full profile settings
        createSignatureSettings(newProfile, gpe);

        var savedGpe = profileGeneralRepository.save(gpe);
        return savedGpe;
    }

    public ProfileGeneralEntity update(GeneralProfileModel updatedProfile, UUID userId) throws Exception {
        var entity = profileGeneralRepository.findById(updatedProfile.getId());
        if(entity.isPresent()) {
            var oldGpe = entity.get();
            if(!oldGpe.getUserId().toString().equals(userId.toString()))
                throw new EntityNotFoundException("Profile not found");

            if(!oldGpe.getName().equals(updatedProfile.getName()))
                oldGpe.setName(updatedProfile.getName());
            if(oldGpe.getHashAlgorithm() != updatedProfile.getHashAlgorithm())
                oldGpe.setHashAlgorithm(updatedProfile.getHashAlgorithm());
            if(oldGpe.getProfileType() != updatedProfile.getProfileType()) {
                // delete old profile
                switch(oldGpe.getProfileType()) {
                    case PAdES:
                        padesRepository.deleteById(oldGpe.getFullProfileId());
                        break;
                    case CAdES:
                        cadesRepository.deleteById(oldGpe.getFullProfileId());
                        break;
                    case XAdES:
                        xadesRepository.deleteById(oldGpe.getFullProfileId());
                }

                // create new profile
                createSignatureSettings(updatedProfile, oldGpe);
            } else {
                // check if are any updates is same profile
                switch (oldGpe.getProfileType()) {
                    case CAdES:
                        updateCadesSettings(oldGpe.getFullProfileId(), updatedProfile.getCadesProfile());
                        break;
                    case PAdES:
                        updatePadesSettings(oldGpe.getFullProfileId(), updatedProfile.getPadesProfile());
                        break;
                    case XAdES:
                        updateXadesSettings(oldGpe.getFullProfileId(), updatedProfile.getXadesProfile());
                        break;
                }
            }

            if(updatedProfile.getSignerInfo() != null) {
                if (oldGpe.getSignerInfoId() == null) {
                    // create
                    createSignerInfo(updatedProfile, oldGpe);
                } else {
                    // update
                    updateSignerInfo(updatedProfile, oldGpe);
                }
            }
            else if(updatedProfile.getSignerInfo() == null && oldGpe.getSignerInfoId() != null) {
                // delete signer info
                signerInfoRepository.deleteById(oldGpe.getSignerInfoId());
                oldGpe.setSignerInfoId(null);
            }

            var newGpe = profileGeneralRepository.save(oldGpe);
            return newGpe;
        } else throw new EntityNotFoundException("ProfileGeneralEntity with id = " + updatedProfile.getId() + " not found");
    }

    public boolean delete(long profileId, UUID userId) {
        var entity = profileGeneralRepository.findById(profileId);
        if (entity.isPresent()) {
            var gpe= entity.get();

            if(!gpe.getUserId().equals(userId))
                throw new EntityNotFoundException("ProfileGeneralEntity with id = " + profileId + " not found");

            // delete signer info id available
            if(gpe.getSignerInfoId() != null)
                signerInfoRepository.deleteById(gpe.getSignerInfoId());

            // delete profile settings
            var fullProfileId = gpe.getFullProfileId();
            switch (gpe.getProfileType()) {
                case XAdES:
                    xadesRepository.deleteById(fullProfileId);
                    break;
                case CAdES:
                    cadesRepository.deleteById(fullProfileId);
                    break;
                case PAdES:
                    padesRepository.deleteById(fullProfileId);
                    break;
            }

            // delete general profile
            profileGeneralRepository.delete(entity.get());

            // all deleted
            return true;
        } else throw new EntityNotFoundException("ProfileGeneralEntity with id = " + profileId + " not found");
    }

    public void deleteAllUserProfiles(UUID userId) {
        var profiles = getAllForUser(userId);
        if (profiles != null) {
            var ids = new ArrayList<Long>();
            for (var profile : profiles) {
                ids.add(profile.getId());
            }
            if (ids.size() > 0) {
                for (var profId : ids) {
                    delete(profId, userId);
                }
            }
        }
    }

    //endregion

    //region PrivateMethods

    private void createSignatureSettings(GeneralProfileModel gpm, ProfileGeneralEntity gpe) throws Exception {
        gpe.setProfileType(gpm.getProfileType());
        switch(gpm.getProfileType()) {
            case CAdES:
                if(gpm.getCadesProfile() == null)
                    throw new IllegalArgumentException("CadesSettings are required");
                var profileCadesEntity = createCadesSettings(gpm.getCadesProfile());
                gpe.setFullProfileId(profileCadesEntity.getId());
                break;
            case XAdES:
                if(gpm.getXadesProfile() == null)
                    throw new IllegalArgumentException("XadesSettings are required");
                var profileXadesEntity = createXadesSettings(gpm.getXadesProfile());
                gpe.setFullProfileId(profileXadesEntity.getId());
                break;
            case PAdES:
                if(gpm.getPadesProfile() == null)
                    throw new IllegalArgumentException("PadesSettings are required");
                var profilePadesEntity = createPadesSettings(gpm.getPadesProfile());
                gpe.setFullProfileId(profilePadesEntity.getId());
                break;
            default:
                throw new Exception("Unsupported sign type"); // should not obtain
        }
    }

    //region CAdES

    private ProfileCades getCadesSettings(long fullProfileId) {
        var cadesEnt = cadesRepository.findById(fullProfileId);
        if (cadesEnt.isPresent()) {
            var cadesData = cadesEnt.get();
            var cadesModel = new ProfileCades();

            // map entity to model
            cadesModel.setId(cadesModel.getId());
            cadesModel.setCadesType(cadesData.getCadesType());
            cadesModel.setDetached(cadesData.isDetached());

            return cadesModel;
        } else return null;
    }

    private ProfileCadesEntity createCadesSettings(ProfileCades pc) {
        var pce = new ProfileCadesEntity();

        // map model to entity
        pce.setDetached(pc.isDetached());
        pce.setCadesType(pc.getCadesType());

        var savedCs = cadesRepository.save(pce);
        return savedCs;
    }

    private void updateCadesSettings(long fullProfileId, ProfileCades newPc) {
        var cadesEnt = cadesRepository.findById(fullProfileId);
        if(cadesEnt.isPresent()) {
            var oldPce = cadesEnt.get();

            if(oldPce.isDetached() != newPc.isDetached())
                oldPce.setDetached(newPc.isDetached());
            if(oldPce.getCadesType() != newPc.getCadesType())
                oldPce.setCadesType(newPc.getCadesType());

            cadesRepository.save(oldPce);
        } else throw new EntityNotFoundException("ProfileCadesEntity with id = " + fullProfileId + " not found");
    }

    //endregion

    //region PAdES

    private ProfilePades getPadesSettings(long fullProfileId) {
        var padesEnt = padesRepository.findById(fullProfileId);
        if(padesEnt.isPresent()) {
            var ppe = padesEnt.get();
            var pp = new ProfilePades();

            // map entity to model
            pp.setId(ppe.getId());
            pp.setPadesType(ppe.getPadesType());
            pp.setVisible(ppe.isVisible());
            pp.setVisibleSignatureType(ppe.getVisibleSignatureType());
            pp.setPos_x(ppe.getPos_x());
            pp.setPos_y(ppe.getPos_y());
            pp.setWidth(ppe.getWidth());
            pp.setHeight(ppe.getHeight());
            pp.setPage(ppe.getPage());
            pp.setSignatureImage(ppe.getSignatureImage());
            pp.setSignatureText(ppe.getSignatureText());
            pp.setBackgroundColor(ppe.getBackgroundColor());
            pp.setSignerName(ppe.getSignerName());
            pp.setSignLocation(ppe.getSignLocation());
            pp.setSignReason(ppe.getSignReason());

            return pp;
        } else return null;
    }

    private ProfilePadesEntity createPadesSettings(ProfilePades pp) {
        var ppe = new ProfilePadesEntity();

        // map model to entity
        ppe.setPadesType(pp.getPadesType());
        ppe.setVisible(pp.isVisible());
        ppe.setVisibleSignatureType(pp.getVisibleSignatureType());
        ppe.setSignatureImage(pp.getSignatureImage());
        ppe.setPos_x(pp.getPos_x());
        ppe.setPos_y(pp.getPos_y());
        ppe.setWidth(pp.getWidth());
        ppe.setHeight(pp.getHeight());
        ppe.setPage(pp.getPage());
        ppe.setSignatureText(pp.getSignatureText());
        ppe.setBackgroundColor(pp.getBackgroundColor());
        ppe.setSignerName(pp.getSignerName());
        ppe.setSignLocation(pp.getSignLocation());
        ppe.setSignReason(pp.getSignReason());

        var savedPpe = padesRepository.save(ppe);
        return savedPpe;
    }

    private void updatePadesSettings(long fullProfileId, ProfilePades newPp) {
        var padesEntity = padesRepository.findById(fullProfileId);
        if(padesEntity.isPresent()) {
            var oldPpe = padesEntity.get();

            if(oldPpe.getPadesType() != newPp.getPadesType())
                oldPpe.setPadesType(newPp.getPadesType());
            if(oldPpe.isVisible() != newPp.isVisible())
                oldPpe.setVisible(newPp.isVisible());
            if(oldPpe.getVisibleSignatureType() != newPp.getVisibleSignatureType())
                oldPpe.setVisibleSignatureType(newPp.getVisibleSignatureType());

            // images will not be compared because of their size, which can be quite big
            if(newPp.getSignatureImage() != null)
                oldPpe.setSignatureImage(newPp.getSignatureImage());

            if(oldPpe.getPos_x() == null && newPp.getPos_x() != null)
                oldPpe.setPos_x(newPp.getPos_x());
            else if(oldPpe.getPos_x() != null && !oldPpe.getPos_x().equals(newPp.getPos_x()))
                oldPpe.setPos_x(newPp.getPos_x());

            if(oldPpe.getPos_y() == null && newPp.getPos_y() != null)
                oldPpe.setPos_y(newPp.getPos_y());
            else if(oldPpe.getPos_y() != null && !oldPpe.getPos_y().equals(newPp.getPos_y()))
                oldPpe.setPos_y(newPp.getPos_y());

            if(oldPpe.getWidth() == null && newPp.getWidth() != null)
                oldPpe.setWidth(newPp.getWidth());
            else if(oldPpe.getWidth() != null && !oldPpe.getWidth().equals(newPp.getWidth()))
                oldPpe.setWidth(newPp.getWidth());

            if(oldPpe.getHeight() == null && newPp.getHeight() != null)
                oldPpe.setHeight(newPp.getHeight());
            else if(oldPpe.getHeight() != null && !oldPpe.getHeight().equals(newPp.getHeight()))
                oldPpe.setHeight(newPp.getHeight());

            if(oldPpe.getPage() == null && newPp.getPage() != null)
                oldPpe.setPage(newPp.getPage());
            else if(oldPpe.getPage() != null && !oldPpe.getPage().equals(newPp.getPage()))
                oldPpe.setPage(newPp.getPage());

            if(oldPpe.getSignatureText() == null && newPp.getSignatureText() != null)
                oldPpe.setSignatureText(newPp.getSignatureText());
            else if(oldPpe.getSignatureText() != null && !oldPpe.getSignatureText().equals(newPp.getSignatureText()))
                oldPpe.setSignatureText(newPp.getSignatureText());

            if(oldPpe.getBackgroundColor() == null && newPp.getBackgroundColor() != null)
                oldPpe.setBackgroundColor(newPp.getBackgroundColor());
            else if(oldPpe.getBackgroundColor() != null && !oldPpe.getBackgroundColor().equals(newPp.getBackgroundColor()))
                oldPpe.setBackgroundColor(newPp.getBackgroundColor());

            if(oldPpe.getSignerName() == null && newPp.getSignerName() != null)
                oldPpe.setSignerName(newPp.getSignerName());
            else if(oldPpe.getSignerName() != null && !oldPpe.getSignerName().equals(newPp.getSignerName()))
                oldPpe.setSignerName(newPp.getSignerName());

            if(oldPpe.getSignLocation() == null && newPp.getSignLocation() != null)
                oldPpe.setSignLocation(newPp.getSignLocation());
            else if(oldPpe.getSignLocation() != null && !oldPpe.getSignLocation().equals(newPp.getSignLocation()))
                oldPpe.setSignLocation(newPp.getSignLocation());

            if(oldPpe.getSignReason() == null && newPp.getSignReason() != null)
                oldPpe.setSignReason(newPp.getSignReason());
            else if(oldPpe.getSignReason() != null && !oldPpe.getSignReason().equals(newPp.getSignReason()))
                oldPpe.setSignReason(newPp.getSignReason());

            padesRepository.save(oldPpe);
        } else throw new EntityNotFoundException("ProfilePadesEntity with id = " + fullProfileId + " not found");
    }

    //endregion

    //region XAdES

    private ProfileXades getXadesSettings(long fullProfileId) {
        var xadesEnt = xadesRepository.findById(fullProfileId);
        if(xadesEnt.isPresent()) {
            var xadesData = xadesEnt.get();
            var xadesModel = new ProfileXades();

            // Map entity to model
            xadesModel.setId(xadesModel.getId());
            xadesModel.setXadesType(xadesData.getXadesType());
            xadesModel.setDetached(xadesData.isDetached());

            return xadesModel;
        } else return null;
    }

    private ProfileXadesEntity createXadesSettings(ProfileXades px) {
        var pxe = new ProfileXadesEntity();

        // map model to entity
        pxe.setXadesType(px.getXadesType());
        pxe.setDetached(px.isDetached());

        var savedPxe = xadesRepository.save(pxe);
        return savedPxe;
    }

    private void updateXadesSettings(long fullProfileId, ProfileXades newPx) {
        var entity = xadesRepository.findById(fullProfileId);
        if(entity.isPresent()) {
            var oldPxe = entity.get();

            if(oldPxe.isDetached() != newPx.isDetached())
                oldPxe.setDetached(newPx.isDetached());
            if(oldPxe.getXadesType() != newPx.getXadesType())
                oldPxe.setXadesType(newPx.getXadesType());

            xadesRepository.save(oldPxe);
        } else throw new EntityNotFoundException("ProfileXadesEntity with id = " + fullProfileId + " not found");
    }

    //endregion

    //region SignerInfo

    private void createSignerInfo(GeneralProfileModel gp, ProfileGeneralEntity gpe) {
        // create entity
        var si = gp.getSignerInfo();
        var sie = new SignerInfoEntity();
        sie.setCountry(si.getCountry());
        sie.setProvince(si.getProvince());
        sie.setPostalCode(si.getPostalCode());
        sie.setCity(si.getCity());
        sie.setRole(si.getRole());
        sie.setProofOfOrigin(si.isProofOfOrigin());
        sie.setProofOfCreation(si.isProofOfCreation());

        // save entity
        var savedSi =  signerInfoRepository.save(sie);
        // add id to generalProfile
        gpe.setSignerInfoId(savedSi.getId());
    }

    private void updateSignerInfo(GeneralProfileModel gp, ProfileGeneralEntity gpe) {
        var newSi = gp.getSignerInfo();
        var oldSiEnt = signerInfoRepository.findById(gpe.getSignerInfoId());
        if(oldSiEnt.isPresent()) {
            var oldSi = oldSiEnt.get();

            if(oldSi.getCountry() == null && newSi.getCountry() != null)
                oldSi.setCountry(newSi.getCountry());
            else if(oldSi.getCountry() != null && !oldSi.getCountry().equals(newSi.getCountry()))
                oldSi.setCountry(newSi.getCountry());

            if(oldSi.getProvince() == null && newSi.getProvince() != null)
                oldSi.setProvince(newSi.getProvince());
            else if(oldSi.getProvince() != null && !oldSi.getProvince().equals(newSi.getProvince()))
                oldSi.setProvince(newSi.getProvince());

            if(oldSi.getPostalCode() == null && newSi.getPostalCode() != null)
                oldSi.setPostalCode(newSi.getPostalCode());
            else if(oldSi.getPostalCode() != null && !oldSi.getPostalCode().equals(newSi.getPostalCode()))
                oldSi.setPostalCode(newSi.getPostalCode());

            if(oldSi.getCity() == null && newSi.getCity() != null)
                oldSi.setCity(newSi.getCity());
            else if(oldSi.getCity() != null && !oldSi.getCity().equals(newSi.getCity()))
                oldSi.setCity(newSi.getCity());

            if(oldSi.getRole() == null && newSi.getRole() != null)
                oldSi.setRole(newSi.getRole());
            else if(oldSi.getRole() != null && !oldSi.getRole().equals(newSi.getRole()))
                oldSi.setRole(newSi.getRole());

            if(oldSi.isProofOfCreation() != newSi.isProofOfCreation())
                oldSi.setProofOfCreation(newSi.isProofOfCreation());

            if(oldSi.isProofOfOrigin() != newSi.isProofOfOrigin())
                oldSi.setProofOfOrigin(newSi.isProofOfOrigin());

            signerInfoRepository.save(oldSi);
        }
    }

    //endregion

    //endregion

}

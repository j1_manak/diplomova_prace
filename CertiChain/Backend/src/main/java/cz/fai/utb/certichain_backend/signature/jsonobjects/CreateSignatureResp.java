package cz.fai.utb.certichain_backend.signature.jsonobjects;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class CreateSignatureResp implements Serializable {

    @Getter @Setter private byte[] signature;
    @Getter @Setter private String signatureMimeType;
    @Getter @Setter private String signatureFileExtensions;
    @Getter @Setter private boolean addedToBlockchain;
    @Getter @Setter private String addToBlockchainError;
}

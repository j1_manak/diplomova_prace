package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class VerifyDocumentItem implements Serializable {
    @Getter @Setter private String collection;
    @Getter @Setter private String scope;
    @Getter @Setter private ObjectId provenDbId;
    @Getter @Setter private ObjectId documentId;
    @Getter @Setter private String versionProofId;
    @Getter @Setter private String status;
    @Getter @Setter private String documentHash;
    @Getter @Setter private String versionHash;
    @Getter @Setter private VerifyDocumentProofDetails proof;
}

package cz.fai.utb.certichain_backend.profiles.enums;

public enum HashAlgorithm {
    SHA1,
    SHA256,
    SHA384,
    SHA512
}

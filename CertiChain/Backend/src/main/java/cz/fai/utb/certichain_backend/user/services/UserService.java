package cz.fai.utb.certichain_backend.user.services;

import cz.fai.utb.certichain_backend.blockchain.services.BlockChainService;
import cz.fai.utb.certichain_backend.profiles.services.ProfileGeneralService;
import cz.fai.utb.certichain_backend.user.entitities.UserEntity;
import cz.fai.utb.certichain_backend.user.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserService implements UserDetailsService {

    private UserRepository userRepository;
    private BlockChainService blockChainService;
    private ProfileGeneralService profileService;

    @Autowired
    public UserService(UserRepository userRepository, BlockChainService blockChainService, ProfileGeneralService profileService) {
        this.userRepository = userRepository;
        this.blockChainService = blockChainService;
        this.profileService = profileService;
    }

    public List<UserEntity> getAllUsers() {
        return userRepository.findAll();
    }

    public UserEntity getUserById(UUID id) {
        var users = userRepository.findAll();
        for(var user : users) {
            if(user.getPublicId().equals(id) && !user.isDeleted()) return user;
        }
        return null;
    }

    public UserEntity getUserByEmail(String email) {
        var users = userRepository.findByEmail(email);
        for(var user : users) {
            if(!user.isDeleted()) return user;
        }
        return null;
    }

    public UserEntity addUser(UserEntity newUser) {
        try {
            userRepository.save(newUser);
            return newUser;
        } catch (Throwable ex) {
            return null;
        }
    }

    public UserEntity updateUser(UserEntity updatedUser) {
        userRepository.save(updatedUser);
        return updatedUser;
    }

    public boolean deleteUser(UserEntity user){
        user.setDeleted(true);
        blockChainService.deleteUserRecords(user.getPublicId());
        profileService.deleteAllUserProfiles(user.getPublicId());
        userRepository.save(user);
        return true;
    }

    public UserEntity updatePassword(UserEntity user, String newPwdHash) {
        user.setPasswordHash(newPwdHash);
        userRepository.save(user);
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = getUserByEmail(username);
        if(user == null) {
            throw  new UsernameNotFoundException("user not found");
        }
        return new User(user.getEmail(), user.getPasswordHash(), new ArrayList<>());
    }
}

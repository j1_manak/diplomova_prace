package cz.fai.utb.certichain_backend.signature.utils;

import eu.europa.esig.dss.token.KSPrivateKeyEntry;

import java.io.ByteArrayInputStream;
import java.security.KeyStore;

public class CertUtils {

    public static KSPrivateKeyEntry getSigningCertificate(byte[] certPfx, String certPwd) throws Exception {
        var keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(new ByteArrayInputStream(certPfx), certPwd.toCharArray());
        var aliases = keyStore.aliases();
        var alias = aliases.nextElement();
        var keyStoreEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(alias, new KeyStore.PasswordProtection(certPwd.toCharArray()));
        return new KSPrivateKeyEntry(alias, keyStoreEntry);
    }

}

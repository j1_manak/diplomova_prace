package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

public class UserBlockchainRecord implements Serializable {
    @Getter @Setter private int id;
    @Getter @Setter private String creationDate;
    @Getter @Setter private String hash;
    @Getter @Setter private UUID proofId;
}

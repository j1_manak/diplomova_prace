package cz.fai.utb.certichain_backend.profiles.jsonobjects;

import cz.fai.utb.certichain_backend.profiles.enums.XadesTypes;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ProfileXades implements Serializable {

    @Getter @Setter private long id;
    @Getter @Setter private XadesTypes xadesType;
    @Getter @Setter boolean isDetached;
}

package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

public class ProofDocument {
    @Getter @Setter private String name;
    @Getter @Setter private Long documents;
}

package cz.fai.utb.certichain_backend.signature.jsonobjects;

import lombok.Getter;
import lombok.Setter;

public class VerifyInBlockchainRes {
    public VerifyInBlockchainRes() {

    }

    public VerifyInBlockchainRes(String proofId, String submitted, String hash, String status) {
        this.proofId = proofId;
        this.submitted = submitted;
        this.hash = hash;
        this.status = status;
    }

    @Getter @Setter private String proofId;
    @Getter @Setter private String submitted;
    @Getter @Setter private String hash;
    @Getter @Setter private String status;
}

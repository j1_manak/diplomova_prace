package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class ProofDetails {
    @Getter @Setter private String batchId;
    @Getter @Setter private List<ProofDocument> collections;
}

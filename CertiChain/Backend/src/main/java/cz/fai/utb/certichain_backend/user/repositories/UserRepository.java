package cz.fai.utb.certichain_backend.user.repositories;

import cz.fai.utb.certichain_backend.user.entitities.UserEntity;
import cz.fai.utb.certichain_backend.user.services.UserService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    List<UserEntity> findByEmail(String email);
    Optional<UserEntity> findByPublicId(UUID publicId);
}

package cz.fai.utb.certichain_backend.profiles.jsonobjects;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.fai.utb.certichain_backend.profiles.enums.HashAlgorithm;
import cz.fai.utb.certichain_backend.profiles.enums.ProfileTypes;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeneralProfileModel implements Serializable {

    @Getter @Setter private long id;
    @Getter @Setter private String name;
    @Getter @Setter private ProfileTypes profileType;
    @Getter @Setter private HashAlgorithm hashAlgorithm;
    @Getter @Setter private SignerInfoReqModel signerInfo;
    @Getter @Setter private ProfileXades xadesProfile;
    @Getter @Setter private ProfileCades cadesProfile;
    @Getter @Setter private ProfilePades padesProfile;
}

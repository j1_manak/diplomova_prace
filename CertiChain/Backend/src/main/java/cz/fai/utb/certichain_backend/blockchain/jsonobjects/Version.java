package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import cz.fai.utb.certichain_backend.signature.jsonobjects.MyDateTime;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class Version {
    @Getter @Setter private long version;
    @Getter @Setter private String status;
    @Getter @Setter private MyDateTime effectiveDate;
}

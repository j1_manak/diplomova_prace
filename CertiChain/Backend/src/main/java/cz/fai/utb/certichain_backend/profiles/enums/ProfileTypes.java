package cz.fai.utb.certichain_backend.profiles.enums;

public enum ProfileTypes {
    PAdES,
    CAdES,
    XAdES
}

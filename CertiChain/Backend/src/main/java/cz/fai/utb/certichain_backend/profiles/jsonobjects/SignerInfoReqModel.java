package cz.fai.utb.certichain_backend.profiles.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class SignerInfoReqModel implements Serializable {

    @Getter @Setter private long id;
    @Getter @Setter private String country;
    @Getter @Setter private String province;
    @Getter @Setter private String postalCode;
    @Getter @Setter private String city;
    @Getter @Setter private String role;
    @Getter @Setter private boolean isProofOfOrigin;
    @Getter @Setter private boolean isProofOfCreation;
}

package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import cz.fai.utb.certichain_backend.signature.jsonobjects.MyDateTime;
import lombok.Getter;
import lombok.Setter;

public class Proof {
    @Getter @Setter private String proofId;
    @Getter @Setter private long version;
    @Getter @Setter private MyDateTime submitted;
    @Getter @Setter private String hash;
    @Getter @Setter private String status;
    @Getter @Setter private ProofDetails details;
}

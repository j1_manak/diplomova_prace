package cz.fai.utb.certichain_backend.signature.jsonobjects;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

public class MyDateTime {
    @Getter
    @Setter
    @SerializedName("$date")
    private String date;
}

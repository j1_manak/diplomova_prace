package cz.fai.utb.certichain_backend.blockchain.controller;

import cz.fai.utb.certichain_backend.blockchain.jsonobjects.GetUserBlockchainRecords;
import cz.fai.utb.certichain_backend.blockchain.jsonobjects.VerifyRecordResp;
import cz.fai.utb.certichain_backend.error.ErrorReturnCodes;
import cz.fai.utb.certichain_backend.error.dto.ErrorResp;
import cz.fai.utb.certichain_backend.blockchain.services.BlockChainService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/blockchain")
public class BlockchainController {

    private BlockChainService blockChainService;

    public BlockchainController(BlockChainService blockChainService) {

        this.blockChainService = blockChainService;
    }

    @GetMapping(name = "/getUserRecords/{userId}", path = "/getUserRecords/{userId}")
    public ResponseEntity<?> getUserRecords(@PathVariable UUID userId) {
        try {
            if (userId == null) {
                return ResponseEntity
                        .badRequest()
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, "Žádost neobsahuje id uživatele"));
            }

            var items = blockChainService.getUserRecords(userId);
            var respObj = new GetUserBlockchainRecords();
            respObj.setItems(items);
            respObj.setItemsCount(items.size());
            return ResponseEntity.ok(respObj);
        } catch (Throwable ex) {
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, ex.getMessage()));
        }
    }

    @GetMapping(name = "/verifyRecord", path = "/verifyRecord/{recordId}")
    public ResponseEntity<?> verifyRecord(@PathVariable String recordId) {
        try {
            if (recordId == null) {
                return ResponseEntity
                        .badRequest()
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, "Žádost neobsahuje id záznamu"));
            }

            if (recordId.isEmpty() || recordId.isBlank()) {
                return ResponseEntity
                        .badRequest()
                        .body(new ErrorResp(HttpStatus.BAD_REQUEST.value(), ErrorReturnCodes.errBadRequest, "Žádost neobsahuje id záznamu"));
            }

            var proof = blockChainService.getProof(recordId);

            if (proof == null) {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new ErrorResp(HttpStatus.NOT_FOUND.value(), ErrorReturnCodes.errNotFound, "Záznam s id " + recordId + " nebyl nalezen"));
            }
            var respObj = new VerifyRecordResp();
            respObj.setVerifyStatus(proof.getStatus());

            return ResponseEntity.ok(respObj);
        } catch (Exception ex) {
            System.out.println(ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResp(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorReturnCodes.errInternalError, ex.getMessage()));
        }
    }

}

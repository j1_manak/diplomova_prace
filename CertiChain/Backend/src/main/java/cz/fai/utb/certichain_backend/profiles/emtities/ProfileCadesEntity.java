package cz.fai.utb.certichain_backend.profiles.emtities;

import cz.fai.utb.certichain_backend.profiles.enums.CadesTypes;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ProfileCadesEntity {
    @Getter @Setter @Id @GeneratedValue
    private long id;
    @Getter @Setter private CadesTypes cadesType;
    @Getter @Setter private boolean isDetached;
}

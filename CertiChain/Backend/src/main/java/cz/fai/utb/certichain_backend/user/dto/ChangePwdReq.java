package cz.fai.utb.certichain_backend.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ChangePwdReq implements Serializable {

    @Getter
    @Setter
    private String oldPassword;

    @Getter
    @Setter
    private String newPassword;
}

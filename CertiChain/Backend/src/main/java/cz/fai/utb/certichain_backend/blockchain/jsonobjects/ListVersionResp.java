package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class ListVersionResp {
    @Getter @Setter private int ok;
    @Getter @Setter private List<Version> versions;
}

package cz.fai.utb.certichain_backend.blockchain.jsonobjects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

public class GetUserBlockchainRecords implements Serializable {
    @Getter @Setter private int itemsCount;
    @Getter @Setter private List<UserBlockchainRecord> items;
}

package cz.fai.utb.certichain_backend.profiles.emtities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SignerInfoEntity  {
    @Getter @Setter @Id @GeneratedValue
    private long id;
    @Getter @Setter private String country;
    @Getter @Setter private String province;
    @Getter @Setter private String postalCode;
    @Getter @Setter private String city;
    @Getter @Setter private String role;
    @Getter @Setter private boolean isProofOfOrigin;
    @Getter @Setter private boolean isProofOfCreation;
}

package cz.fai.utb.certichain_backend.filters;

import com.fasterxml.jackson.databind.JsonSerializer;
import cz.fai.utb.certichain_backend.error.dto.ErrorResp;
import cz.fai.utb.certichain_backend.user.services.JwtTokenService;
import cz.fai.utb.certichain_backend.user.services.UserService;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    private final UserService userService;
    private final JwtTokenService jwtTokenService;

    @Autowired
    public JwtRequestFilter(UserService userService, JwtTokenService jwtTokenService) {
        this.userService = userService;
        this.jwtTokenService = jwtTokenService;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        var reqTokenHeader = request.getHeader("Authorization");
        var headers = request.getHeaders("access-control-request-headers");

        String username = null;
        String jwtToken = null;
        // JWT is in Bearer form
        try {
            if (reqTokenHeader != null && reqTokenHeader.startsWith("Bearer ")) {
                jwtToken = reqTokenHeader.substring(7);
                username = tryGetUsernameFromHeader(jwtToken, response);
            } else {
                System.out.println("JWT token is not in Bearer form");
                jwtToken = reqTokenHeader;
                username = tryGetUsernameFromHeader(jwtToken, response);
            }

            var auth = SecurityContextHolder.getContext().getAuthentication();

            if(username != null && auth  == null) {
                var userInfo = userService.loadUserByUsername(username);
                if(jwtTokenService.validateToken(jwtToken, userInfo)) {
                    var usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userInfo, null, userInfo.getAuthorities());
                    usernamePasswordAuthenticationToken
                            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
        } catch(ExpiredJwtException e) {
            response.setStatus(403);
            String respJson = "{\"code\": \"403\", \"message\": \"TokenExpired\", \"additionalInfo\": \"Platnost jwt tokenu vypršela\" }";
        }

        filterChain.doFilter(request, response);
    }

    private String tryGetUsernameFromHeader(String authHeader, HttpServletResponse response) {
        try {
            return jwtTokenService.getUsernameFromToken(authHeader);
        } catch(IllegalArgumentException e ) {
            System.out.println("Unable to get JWT token");
            response.setStatus(500);
        } catch(ExpiredJwtException e) {
            response.setStatus(403);
            System.out.println("JWT token has expired");
            throw e;
        }
        return null;
    }
}
